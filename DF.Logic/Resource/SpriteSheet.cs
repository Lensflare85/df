﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Util;
using Windows.Foundation;

namespace DF.Logic.Resource
{
    public sealed class SpriteSheet
    {
        public IBitmap Bitmap { get; private set; }

        public SpriteSheetRegion[] Regions { get; private set; }

        public SpriteSheet(IBitmap bitmap, SpriteSheetRegion[] regions)
        {
            Bitmap = bitmap;
            Regions = regions;
        }
    }

    public struct SpriteSheetRegion
    {
        public IntVector2 Start;
        public IntVector2 End;
        public IntVector2 ReferencePoint;

        public SpriteSheetRegion(int startX, int startY, int endX, int endY, int referencePointX, int referencePointY)
        {
            Start = new IntVector2(startX, startY);
            End = new IntVector2(endX, endY);
            ReferencePoint = new IntVector2(referencePointX, referencePointY);
        }

        public IntVector2 Size() => End - Start;
    }
}
