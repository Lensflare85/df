﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Resource
{
    public sealed class TileWithVariations
    {
        IBitmap[] tiles;

        public TileWithVariations(params IBitmap[] tiles)
        {
            this.tiles = tiles;
        }

        public IBitmap this[int variation]
        {
            get => tiles[variation];
        }

        public int NumberOfVariations => tiles?.Length ?? 0;
    }
}
