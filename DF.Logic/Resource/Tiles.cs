﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Resource
{
    public sealed class Tiles
    {
        public Dictionary<Ground, TileWithVariations> WorldGround { get; private set; } = new Dictionary<Ground, TileWithVariations>();

        public async Task LoadAsync(IResourceCreator resourceCreator, IFileSystem fileSystem)
        {
            var path = Path.Combine(fileSystem.ExecutionDirectory(), "DF.Logic", "gfx");

            var a0 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "a0.png"));
            var a1 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "a1.png"));
            var a2 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "a2.png"));
            var a3 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "a3.png"));
            WorldGround[Ground.Sand] = new TileWithVariations(a0, a1, a2, a3);

            var w0 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "w0.png"));
            var w1 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "w1.png"));
            var w2 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "w2.png"));
            var w3 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "w3.png"));
            var w4 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "w0.png"));
            var w5 = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "w1.png"));
            WorldGround[Ground.Earth] = new TileWithVariations(w0, w1, w2, w3, w4, w5);
        }
    }
}
