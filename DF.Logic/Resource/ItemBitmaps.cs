﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Resource
{
    public sealed class ItemBitmaps
    {
        public IBitmap Wood { get; private set; }

        public async Task LoadAsync(IResourceCreator resourceCreator, IFileSystem fileSystem)
        {
            var path = Path.Combine(fileSystem.ExecutionDirectory(), "DF.Logic", "gfx", "items");

            Wood = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "wood.png"));
        }
    }
}
