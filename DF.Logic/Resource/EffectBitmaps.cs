﻿using DF.Logic.Interfaces;
using System.IO;
using System.Threading.Tasks;

namespace DF.Logic.Resource
{
    public sealed class EffectBitmaps
    {
        public IBitmap Beam { get; private set; }
        public IBitmap Shadow { get; private set; }

        public async Task LoadAsync(IResourceCreator resourceCreator, IFileSystem fileSystem)
        {
            var path = Path.Combine(fileSystem.ExecutionDirectory(), "DF.Logic", "gfx", "effects");

            Beam = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "beam.png"));
            Shadow = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "shadow.png"));
        }
    }
}
