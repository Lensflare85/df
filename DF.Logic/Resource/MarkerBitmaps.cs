﻿using DF.Logic.Interfaces;
using System.IO;
using System.Threading.Tasks;

namespace DF.Logic.Resource
{
    public sealed class MarkerBitmaps
    {
        public IBitmap ItemGrabberInput { get; private set; }
        public IBitmap ItemGrabberOutput { get; private set; }

        public async Task LoadAsync(IResourceCreator resourceCreator, IFileSystem fileSystem)
        {
            var path = Path.Combine(fileSystem.ExecutionDirectory(), "DF.Logic", "gfx", "markers");

            ItemGrabberInput = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "item_grabber_input.png"));
            ItemGrabberOutput = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "item_grabber_output.png"));
        }
    }
}
