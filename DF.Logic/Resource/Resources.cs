﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Resource
{
    public sealed class Resources
    {
        public Bitmaps Bitmaps { get; private set; }

        public async Task LoadAsync(IResourceCreator resourceCreator, IFileSystem fileSystem)
        {
            Bitmaps = new Bitmaps();
            await Bitmaps.LoadAsync(resourceCreator, fileSystem);
        }
    }
}
