﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Resource
{
    public sealed class HudBitmaps
    {
        public IBitmap ScrollArrow { get; private set; }
        public IBitmap ScrollArrowStart { get; private set; }

        public async Task LoadAsync(IResourceCreator resourceCreator, IFileSystem fileSystem)
        {
            var path = Path.Combine(fileSystem.ExecutionDirectory(), "DF.Logic", "gfx", "hud");

            ScrollArrow = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "scroll_arrow.png"));
            ScrollArrowStart = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "scroll_arrow_start.png"));
        }
    }
}
