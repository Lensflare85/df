﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Resource
{
    public sealed class EntityBitmaps
    {
        public BuildingBitmaps Buildings { get; private set; }

        public IBitmap WorkerBot { get; private set; }

        public async Task LoadAsync(IResourceCreator resourceCreator, IFileSystem fileSystem)
        {
            Buildings = new BuildingBitmaps();
            await Buildings.LoadAsync(resourceCreator, fileSystem);

            var path = Path.Combine(fileSystem.ExecutionDirectory(), "DF.Logic", "gfx", "entities");

            WorkerBot = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "bot.png"));
        }
    }
}
