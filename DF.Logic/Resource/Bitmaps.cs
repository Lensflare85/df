﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace DF.Logic.Resource
{
    public sealed class Bitmaps
    {
        public Tiles Tiles { get; private set; }
        public HudBitmaps Hud { get; private set; }
        public SpriteSheet TreesTest { get; private set; }
        public EntityBitmaps Entities { get; private set; }
        public ItemBitmaps Items { get; private set; }
        public EffectBitmaps Effects { get; private set; }
        public MarkerBitmaps Markers { get; private set; }

        public async Task LoadAsync(IResourceCreator resourceCreator, IFileSystem fileSystem)
        {
            Hud = new HudBitmaps();
            await Hud.LoadAsync(resourceCreator, fileSystem);

            Entities = new EntityBitmaps();
            await Entities.LoadAsync(resourceCreator, fileSystem);

            Items = new ItemBitmaps();
            await Items.LoadAsync(resourceCreator, fileSystem);

            Effects = new EffectBitmaps();
            await Effects.LoadAsync(resourceCreator, fileSystem);

            Markers = new MarkerBitmaps();
            await Markers.LoadAsync(resourceCreator, fileSystem);

            Tiles = new Tiles();
            await Tiles.LoadAsync(resourceCreator, fileSystem);

            var path = Path.Combine(fileSystem.ExecutionDirectory(), "DF.Logic", "gfx");

            var treesTestBitmap = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "trees_test.png"));
            TreesTest = new SpriteSheet(treesTestBitmap, new[] {
                new SpriteSheetRegion(0,0,127,145,63,125),
                new SpriteSheetRegion(127,0,127+123,145,185,125),
                new SpriteSheetRegion(127+123,0,127+123+102,145,300,125),
                new SpriteSheetRegion(5,148,117,277,59,252),
                new SpriteSheetRegion(137,146,232,276,186,258),
                new SpriteSheetRegion(255,155,342,277,294,259),
            });
        }
    }
}
