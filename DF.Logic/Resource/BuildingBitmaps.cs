﻿using DF.Logic.Interfaces;
using System.IO;
using System.Threading.Tasks;

namespace DF.Logic.Resource
{
    public sealed class BuildingBitmaps
    {
        public IBitmap Container { get; private set; }
        public IBitmap ItemGrabberBase { get; private set; }
        public IBitmap ItemGrabberHand { get; private set; }

        public async Task LoadAsync(IResourceCreator resourceCreator, IFileSystem fileSystem)
        {
            var path = Path.Combine(fileSystem.ExecutionDirectory(), "DF.Logic", "gfx", "entities", "buildings");

            Container = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "container.png"));
            ItemGrabberBase = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "item_grabber_base.png"));
            ItemGrabberHand = await resourceCreator.CreateBitmapFromFilePathAsync(Path.Combine(path, "item_grabber_hand.png"));
        }
    }
}
