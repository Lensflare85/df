﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace DF.Logic
{
    public sealed class PathFinder : IPathFinder
    {
        public IntVector2[] PossibleOffsets { get; } = new[] { new IntVector2(0, 1), new IntVector2(1, 0), new IntVector2(0, -1), new IntVector2(-1, 0) };
        public IntVector2[] PossibleDiagonalOffsets { get; } = new[] { new IntVector2(1, 1), new IntVector2(1, -1), new IntVector2(-1, 1), new IntVector2(-1, -1) };

        public IEnumerable<IntVector2> FindPath(IntVector2 start, IntVector2 end, bool includingEnd, Func<IntVector2, bool> isPassable)
        {
            int stepsToTry = 1000;

            var passableCache = new Dictionary<IntVector2, bool>();

            bool isPassableCached(IntVector2 location)
            {
                var result = passableCache.GetOrNull(location);
                if (result == null)
                {
                    result = isPassable(location);
                    passableCache[location] = result.Value;
                }
                return result.Value;
            }

            IEnumerable<IntVector2> neighborsOf(IntVector2 location, bool includingDiagonals)
            {
                foreach (var offset in PossibleOffsets)
                {
                    var neighbor = location + offset;
                    if (isPassableCached(neighbor))
                    {
                        yield return neighbor;
                    }
                }
                if (includingDiagonals)
                {
                    foreach (var offset in PossibleDiagonalOffsets)
                    {
                        if (isPassableCached(location + offset) && isPassableCached(location + new IntVector2(offset.X, 0)) && isPassableCached(location + new IntVector2(0, offset.Y)))
                        {
                            yield return location + offset;
                        }
                    }
                }
            }

            if(!isPassableCached(start) || start == end)
            {
                return Enumerable.Empty<IntVector2>();
            }

            if (includingEnd && !isPassableCached(end))
            {
                return Enumerable.Empty<IntVector2>();
            }
            else if (!includingEnd && neighborsOf(end, false).Contains(start))
            {
                return Enumerable.Empty<IntVector2>();
            }

            const float squareRootOf2 = 1.41421356237f;

            float neighborCosts(IntVector2 current, IntVector2 neighbor)
            {
                var diff = current - neighbor;
                return diff.X == 0 || diff.Y == 0 ? 1 : squareRootOf2;
            }

            float estimatedCosts(IntVector2 s, IntVector2 e) {
                //var diff = e - s;
                //return Math.Abs(diff.X) + Math.Abs(diff.Y);
                return s.DistanceTo(e);
            };

            var closed = new List<IntVector2>();
            var open = new List<IntVector2>() { start };
            var from = new Dictionary<IntVector2, IntVector2>();
            var gScore = new Dictionary<IntVector2, float>();
            gScore[start] = 0;
            var fScore = new Dictionary<IntVector2, float>();
            fScore[start] = estimatedCosts(start, end);

            while (open.Any() && stepsToTry > 0)
            {
                var current = open.OrderBy(n => fScore.GetOrDefault(n, float.PositiveInfinity)).First(); //TODO: manually enumerate and find smallest. check if it is faster.
                if (includingEnd && current == end)
                {
                    return ReconstructPath(from, current).Reverse().Skip(1);
                }
                else if (!includingEnd && neighborsOf(end, false).Contains(current))
                {
                    return ReconstructPath(from, current).Reverse().Skip(1);
                }

                open.Remove(current);
                closed.Add(current);

                foreach (var neighbor in neighborsOf(current, true))
                {
                    if (closed.Contains(neighbor))
                    {
                        continue;
                    }
                    var costForNeighbor = neighborCosts(current, neighbor);
                    var tgScore = gScore.GetOrDefault(current, float.PositiveInfinity) + costForNeighbor;
                    if (!open.Contains(neighbor))
                    {
                        open.Add(neighbor);
                    }
                    else if (tgScore >= gScore.GetOrDefault(neighbor, float.PositiveInfinity))
                    {
                        continue;
                    }

                    from[neighbor] = current;
                    gScore[neighbor] = tgScore;
                    fScore[neighbor] = gScore[neighbor] + estimatedCosts(neighbor, end);
                }

                --stepsToTry;
            }

            return Enumerable.Empty<IntVector2>();
        }

        IEnumerable<IntVector2> ReconstructPath(Dictionary<IntVector2, IntVector2> from, IntVector2 current)
        {
            var path = new List<IntVector2>() { current };
            while (from.Keys.Contains(current))
            {
                current = from[current];
                path.Add(current);
            }
            return path;
        }
    }
}
