﻿using DF.Logic.Entities;
using DF.Logic.Entities.Components;
using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Util;
using Util.Map;
using Util.Random;

namespace DF.Logic
{
    public sealed class World
    {
        public int GlobalSeed = 1337;

        public Game Game;

        static IntVector2 chunkSize = new IntVector2(32, 32);
        public static IntVector2 ChunkSize => chunkSize;

        public ChunkMap<Map2<WorldCell>> CellChunks { get; private set; } = new ChunkMap<Map2<WorldCell>>(ChunkSize);

        public ChunkMap<Dictionary<IntVector2, Ground>> VoronoiPointsChunks { get; private set; } = new ChunkMap<Dictionary<IntVector2, Ground>>(ChunkSize);

        public ChunkMap<IBitmap> ChunkBitmaps { get; private set; } = new ChunkMap<IBitmap>(ChunkSize);

        RandomNumberGenerator random = new RandomNumberGenerator(23456);

        public IPathFinder PathFinder { get; private set; }

        public List<Player> RegisteredPlayers = new List<Player>();

        public WorldGenerator Generator { get; private set; }

        public ListDictionary<IntVector2, WorldEntity> Entities { get; private set; } = new ListDictionary<IntVector2, WorldEntity>();

        public List<WorldEntity> EntitiesToDestroy { get; private set; } = new List<WorldEntity>();

        public ListDictionary<Player, WorldEntity> EntitiesMarkedForDestruction { get; private set; } = new ListDictionary<Player, WorldEntity>();

        public Dictionary<Player, IntVector2> HoveredCells = new Dictionary<Player, IntVector2>();
        public Dictionary<Player, IntVector2> ClickedCells = new Dictionary<Player, IntVector2>();

        public List<ItemEntity> ItemEntities { get; private set; } = new List<ItemEntity>();
        public List<WorldEntity> TreeEntities { get; private set; } = new List<WorldEntity>();
        public List<BuildingEntity> ContainerEntities { get; private set; } = new List<BuildingEntity>();

        public ListDictionary<IntVector2, ItemGrabberMarker> ItemGrabberMarkedLocations { get; private set; } = new ListDictionary<IntVector2, ItemGrabberMarker>();

        public World()
        {
            PathFinder = new PathFinder();

            Generator = new WorldGenerator() { World = this };
        }

        private void AddTestEntities()
        {
            foreach (var location in Grid.CoordinatesInRange(new IntVector2(7, 8), new IntVector2(3, 3)))
            {
                var bot = new WorkerBotEntity(random.Next()) {
                    Game = Game,
                    Owner = null,
                    CellLocation = location,
                };
                AddEntity(bot);
            }

            {
                AddEntity(Game.EntityFactory.CreateBuilding(Game.BuildingTypes.Container, new IntVector2(4, 4)));
                AddEntity(Game.EntityFactory.CreateBuilding(Game.BuildingTypes.Container, new IntVector2(6, 6)));
            }

            {
                AddEntity(Game.EntityFactory.CreateBuilding(Game.BuildingTypes.Inserter, new IntVector2(5, 4)));
            }
        }

        public void AddEntity(WorldEntity entity)
        {
            Entities.AddValue(entity.CellLocation, entity);

            if(entity is BuildingEntity buildingEntity && buildingEntity.ItemGrabber != null)
            {
                ItemGrabberMarkedLocations.AddValue(buildingEntity.ItemGrabber.InputMarker.Location, buildingEntity.ItemGrabber.InputMarker);
                ItemGrabberMarkedLocations.AddValue(buildingEntity.ItemGrabber.OutputMarker.Location, buildingEntity.ItemGrabber.OutputMarker);
            }
        }

        public void RemoveEntity(WorldEntity entity)
        {
            var location = entity.CellLocation;

            if (Entities[location] == null)
            {
                throw new Exception($"tried to remove entity {entity} from location {location} with no entities");
            }
            Entities.RemoveValueForKey(location, entity);
            switch(entity)
            {
                case ItemEntity itemEntity:
                    {
                        itemEntity.TargetedByEntity?.CancelPlan(); //TODO: do this in Dispose()
                    }
                    break;
                case RefinableEntity refinableEntity:
                    {
                        refinableEntity.Dispose();
                    }
                    break;
                case BuildingEntity buildingEntity:
                    {
                        buildingEntity.Dispose();
                    }
                    break;
            }

            EntitiesMarkedForDestruction.RemoveValue(entity);
        }

        public IEnumerable<(IntVector2 cellCoordinates, Ground ground)> GetAllVoronoiPoints()
        {
            foreach(var chunk in VoronoiPointsChunks.AllChunks())
            {
                foreach(var points in chunk.Value)
                {
                    yield return (points.Key, points.Value);
                }
            }
        }

        public IntVector2 ChunkFromCell(IntVector2 cell)
        {
            return CellChunks.ChunkFromCell(cell);
        }

        public WorldCell? GetCell(IntVector2 coordinates)
        {
            var chunkCoordinates = CellChunks.ChunkFromCell(coordinates);
            var offset = CellChunks.CellInChunk(chunkCoordinates, coordinates);
            var cell = CellChunks[chunkCoordinates]?[offset];
            return cell;
        }

        private void SetCell(IntVector2 coordinates, WorldCell cell)
        {
            var chunkCoordinates = CellChunks.ChunkFromCell(coordinates);
            var chunk = CellChunks[chunkCoordinates];
            if (chunk != null)
            {
                var offset = CellChunks.CellInChunk(chunkCoordinates, coordinates);
                chunk[offset] = cell;
            }
        }

        public static bool CellIsNeighbor(IntVector2 cellPosition1, IntVector2 cellPosition2)
        {
            int ds = cellPosition1.DistanceSquaredTo(cellPosition2);
            return ds > 0 && ds < 3;
        }

        public bool CellIsPassableForEntity(IntVector2 location, WorldEntity entity)
        {
            var entities = Entities[location];

            bool allEntitiesArePassable(IEnumerable<WorldEntity> ent)
            {
                foreach (var e in ent)
                {
                    if (!e.Passable) return false;
                }
                return true;
            }

            return entities == null || !entities.Any() || entities.Contains(entity) || allEntitiesArePassable(entities);
        }

        void ReassignEntityCellPosition(MobileEntity entity, IntVector2 oldPosition)
        {
            var offsetRange2 = WorldEntity.OffsetRange * 2;

            var change = false;
            IntVector2 newPosition = oldPosition;

            if (entity.Offset.X >= WorldEntity.OffsetRange)
            {
                change = true;
                entity.Offset -= new IntVector2(offsetRange2, 0);
                newPosition = oldPosition + IntVector2.UnitX;
            }
            else if (entity.Offset.X <= -WorldEntity.OffsetRange)
            {
                change = true;
                entity.Offset += new IntVector2(offsetRange2, 0);
                newPosition = oldPosition - IntVector2.UnitX;
            }
            else if (entity.Offset.Y >= WorldEntity.OffsetRange)
            {
                change = true;
                entity.Offset -= new IntVector2(0, offsetRange2);
                newPosition = oldPosition + IntVector2.UnitY;
            }
            else if (entity.Offset.Y <= -WorldEntity.OffsetRange)
            {
                change = true;
                entity.Offset += new IntVector2(0, offsetRange2);
                newPosition = oldPosition - IntVector2.UnitY;
            }

            if(change)
            {
                RemoveEntity(entity);
                entity.HandleNewCellLocation(newPosition);
                AddEntity(entity);
                //Debug.WriteLine($"old: {oldPosition}, new {newPosition}");
            }
        }

        bool workerBotsAdded = false;

        public void Update(TimeSpan elapsedTime)
        {
            if(!workerBotsAdded)
            {
                AddTestEntities();
                workerBotsAdded = true;
            }

            ItemEntities.Clear();
            TreeEntities.Clear();
            ContainerEntities.Clear();

            var mobileEntities = new List<MobileEntity>();

            foreach (var entities in Entities.Enumerate())
            {
                var cellLocation = entities.Key;

                if (entities.Value == null)
                {
                    throw new Exception($"entities at location {cellLocation} are null");
                }
                foreach (var entity in entities.Value)
                {
                    switch (entity)
                    {
                        case MobileEntity mobileEntity:
                            mobileEntities.Add(mobileEntity);
                            break;
                        case ItemEntity itemEntity:
                            ItemEntities.Add(itemEntity);
                            break;
                        case RefinableEntity treeEntity when treeEntity.RefinableType == Game.RefinableTypes.Tree:
                            TreeEntities.Add(treeEntity);
                            break;
                        case BuildingEntity buildingEntity when buildingEntity.BuildingType == Game.BuildingTypes.Container:
                            ContainerEntities.Add(buildingEntity);
                            break;
                    }
                }
            }

            foreach (var entities in Entities.Enumerate())
            {
                var cellLocation = entities.Key;

                if (entities.Value == null)
                {
                    throw new Exception($"entities at location {cellLocation} are null");
                }
                foreach (var entity in entities.Value)
                {
                    entity.Update(elapsedTime);
                }
            }

            foreach (var entity in mobileEntities)
            {
                ReassignEntityCellPosition(entity, entity.CellLocation);
            }

            foreach(var entity in EntitiesToDestroy)
            {
                var targetedBy = (entity as RefinableEntity)?.IsTargetOfEntities.ToArray();

                RemoveEntity(entity);

                if (entity is RefinableEntity refinableEntity && refinableEntity.RefinableType == Game.RefinableTypes.Tree)
                {
                    var wood = new ItemEntity(Game.ItemTypes.Wood)
                    {
                        Game = Game,
                        CellLocation = entity.CellLocation,
                        Offset = entity.Offset,
                    };
                    AddEntity(wood);

                    var firstTargetetBy = targetedBy?.FirstOrDefault();
                    if (firstTargetetBy?.Inventory?.CanBeAddedAny(wood.ItemType) ?? false)
                    {
                        firstTargetetBy.AssignPickupItemPlan(wood);
                    }

                    if (false)
                    {
                        foreach (var mobileEntity in mobileEntities)
                        {
                            if (mobileEntity is WorkerBotEntity && mobileEntity.PlannedAction == null && !mobileEntity.PlannedPath.Any())
                            {
                                mobileEntity.AssignPickupItemPlan(wood);
                                break;
                            }
                        }
                    }
                }
            }
            EntitiesToDestroy.Clear();

            foreach(var clickedCellTuple in ClickedCells)
            {
                var clickingPlayer = clickedCellTuple.Key;
                var clickedCell = clickedCellTuple.Value;

                var clickedEntities = Entities[clickedCell];
                if (clickedEntities != null)
                {
                    var removed = EntitiesMarkedForDestruction.RemoveValues(clickedEntities);
                    foreach(var pair in removed.Enumerate())
                    {
                        foreach(var entity in pair.Value)
                        {
                            if(entity is RefinableEntity refinableEntity)
                            {
                                foreach(var targetingEntity in refinableEntity.IsTargetOfEntities.ToArray())
                                {
                                    targetingEntity.CancelPlan();
                                }
                            }
                        }
                    }
                    var cancelledDestruction = !removed.IsEmpty();

                    if (!cancelledDestruction)
                    {
                        var refinableEntities = clickedEntities.Where(entity => entity is RefinableEntity);
                        EntitiesMarkedForDestruction.AddValues(clickingPlayer, refinableEntities);
                    }
                }
            }
            ClickedCells.Clear();
        }
    }
}
