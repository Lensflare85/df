﻿using DF.Logic.Entities;
using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Util;
using Windows.UI;

namespace DF.Logic.View
{
    public sealed class MainWorldView : WorldView
    {
        Color borderColor = Colors.LimeGreen;
        Color hoverColor = Color.FromArgb(50, 255, 255, 255);

        protected override void DrawHoveredCell()
        {
            if(IsHovored) //TODO: make IsHovered player based?
            {
                var playerHovoredCell = GameView.Game.World.HoveredCells.GetOrNull(GameView.Game.Player);
                if (playerHovoredCell != null)
                {
                    var cellLocation = playerHovoredCell.Value;

                    var p = vpcMinusZso + cellLocation * zoomedCellSize;

                    GameView.Renderer.FillRectangle(p, zoomedCellSize, hoverColor);

                    var ground = GameView.Game.World.GetCell(cellLocation)?.Ground ?? Ground.None;
                    if (ground == Ground.None)
                    {
                        var chunkCoordinates = GameView.Game.World.ChunkFromCell(cellLocation);
                        GameView.Game.World.Generator.GenerateCellChunk(chunkCoordinates);
                    }
                }
            }
        }

        protected override void DrawBorder()
        {
            //GameView.Renderer.DrawRectangle(viewPortLocation, viewPortSize, borderColor, 1);
        }

        protected override void DrawMainWorldViewFrame()
        {
            
        }

        public override void Update(TimeSpan elapsedTime)
        {
            if (IsDragging && GameView.IndirectScrollHolding)
            {
                var delta = GameView.CurrentPointerPosition - GameView.IndirectScrollStartPosition;
                ViewPort.SpaceOffset += delta / ViewPort.SpaceZoom * (float)elapsedTime.TotalSeconds * 10;
                GameView.MinimapView.ViewPort.SpaceOffset = ViewPort.SpaceOffset;
            }

            destructionSelectionAnimationTime += (float)elapsedTime.TotalSeconds;
            if(destructionSelectionAnimationTime >= destructionSelectionAnimationInterval)
            {
                destructionSelectionAnimationTime -= destructionSelectionAnimationInterval;
            }
        }

        public override bool HandleHoverEvent(Vector2 pointerPosition)
        {
            IsHovored = GameView.UsingMousePointer && pointerPosition.IsInsideRect(ViewPort.Location, ViewPort.Size);
            if (IsHovored)
            {
                if (GameView.DirectScrollHolding)
                {
                    var delta = pointerPosition - GameView.CurrentPointerPosition;
                    ViewPort.SpaceOffset -= delta / ViewPort.SpaceZoom;
                    GameView.MinimapView.ViewPort.SpaceOffset = ViewPort.SpaceOffset;
                    IsDragging = true;
                }
                else if (GameView.IndirectScrollHolding)
                {
                    //var delta = pointerPosition - GameView.IndirectScrollStartPosition;
                    IsDragging = true;
                }
                else
                {
                    var hoveredCellPosition = CellFromScreen(pointerPosition);
                    GameView.Game.World.HoveredCells[GameView.Game.Player] = hoveredCellPosition;
                }
            }
            return true;
        }

        public override bool HandleLeftClick(Vector2 pointerPosition)
        {
            var clickedCell = CellFromScreen(pointerPosition);
            Debug.WriteLine($"left click cell {clickedCell}");

            var world = GameView.Game.World;

            var clickedEntities = world.Entities[clickedCell];
            if (clickedEntities != null)
            {
                foreach (var e in clickedEntities)
                {
                    var inventory = e?.Inventory;
                    if (inventory != null)
                    {
                        Debug.WriteLine($"{e.GetType().Name} {inventory}");
                    }
                }
            }

            if (false)
            {
                foreach (var entities in world.Entities.Enumerate())
                {
                    foreach (var entity in entities.Value)
                    {
                        if (entity is WorkerBotEntity workerBot)
                        {
                            var from = workerBot.CellLocation;
                            var to = clickedCell;
                            var path = world.PathFinder.FindPath(from, to, true, cellLocation => world.CellIsPassableForEntity(cellLocation, workerBot));
                            workerBot.CancelPlan();
                            workerBot.PlannedPath = new LinkedList<IntVector2>(path);
                            return true;
                        }
                    }
                }
            }

            if(true)
            {
                GameView.Game.World.ClickedCells[GameView.Game.Player] = clickedCell;
            }

            return true;
        }
    }
}
