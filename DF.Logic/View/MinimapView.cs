﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Util;
using Windows.UI;

namespace DF.Logic.View
{
    public sealed class MinimapView : WorldView
    {
        Color borderColor = Colors.Gray;
        Color MainWorldViewFrameColor = Colors.White;

        IntVector2 hoveredCellPosition;

        protected override void DrawWorldCells(IntVector2 startCell, IntVector2 cellRange)
        {
            var startChunk = GameView.Game.World.ChunkFromCell(startCell);
            var endChunk = GameView.Game.World.ChunkFromCell(startCell + cellRange);
            var chunkRange = endChunk - startChunk + IntVector2.One;

            var zoomedCellChunkSize = zoomedCellSize * World.ChunkSize;

            GameView.Renderer.BeginSpriteBatch();

            foreach (var chunkLocation in Grid.CoordinatesInRange(startChunk, chunkRange))
            {
                var p = vpcMinusZso + chunkLocation * zoomedCellChunkSize;

                var ground = GameView.Game.World.GetCell(chunkLocation)?.Ground ?? Ground.None;

                /*
                Color groundColor;
                switch (ground)
                {
                    case Ground.Earth: groundColor = Colors.Brown; break;
                    case Ground.Grass: groundColor = Colors.DarkGreen; break;
                    case Ground.Sand: groundColor = Colors.SandyBrown; break;
                }*/

                var chunkBitmap = GameView.Game.World.ChunkBitmaps[chunkLocation];
                if (chunkBitmap != null)
                {
                    GameView.Renderer.DrawBitmapFromSpriteBatch(chunkBitmap, p, zoomedCellChunkSize + Vector2.One);
                }
            }

            GameView.Renderer.EndSpriteBatch();
        }

        protected override void DrawHoveredCell()
        {
            if (IsHovored)
            {
                var p = vpcMinusZso + hoveredCellPosition * zoomedCellSize;
                GameView.Renderer.FillRectangle(p, zoomedCellSize, Colors.Red);
            }
        }

        protected override void DrawBorder()
        {
            GameView.Renderer.DrawRectangle(viewPortLocation, viewPortSize, borderColor, 4);
        }

        protected override void DrawMainWorldViewFrame()
        {
            var (topLeft, bottomDown) = GameView.MainWorldView.VisualWorldBounds();
            var start = ScreenFromWorld(topLeft);
            var end = ScreenFromWorld(bottomDown);

            GameView.Renderer.DrawRectangle(start, end - start, MainWorldViewFrameColor, 2);
        }

        protected override void DrawGrid(IntVector2 startCell, IntVector2 endCell)
        {

        }

        protected override void DrawEntities(IntVector2 startCell, IntVector2 cellRange)
        {

        }

        public override void Update(TimeSpan elapsedTime)
        {
            
        }

        public override bool HandleHoverEvent(Vector2 pointerPosition)
        {
            IsHovored = (GameView.UsingMousePointer && pointerPosition.IsInsideRect(ViewPort.Location, ViewPort.Size));
            if (IsHovored || IsDragging)
            {
                if (GameView.DirectScrollHolding)
                {
                    var delta = pointerPosition - GameView.CurrentPointerPosition;
                    ViewPort.SpaceOffset -= delta / ViewPort.SpaceZoom;
                    GameView.MainWorldView.ViewPort.SpaceOffset = ViewPort.SpaceOffset;
                    IsDragging = true;
                }
                else if (GameView.IndirectScrollHolding)
                {
                    IsDragging = true;
                }
                else
                {
                    hoveredCellPosition = CellFromScreen(pointerPosition);
                }
            }
            return IsHovored || IsDragging;
        }

        public override bool HandleLeftClick(Vector2 pointerPosition)
        {
            if (ViewPort.Rect.Contains(pointerPosition.ToPoint()))
            {
                var clickedCell = CellFromScreen(pointerPosition);
                Debug.WriteLine($"left click minimap cell {clickedCell}");
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
