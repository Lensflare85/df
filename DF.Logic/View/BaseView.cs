﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.View
{
    public abstract class BaseView
    {
        public bool IsHovored;

        public bool IsDragging = false;

        public abstract bool HandleLeftClick(Vector2 pointerPosition);

        public abstract bool HandleHoverEvent(Vector2 pointerPosition);

        public abstract void Update(TimeSpan elapsedTime);
    }
}
