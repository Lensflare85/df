﻿using DF.Logic.Interfaces;
using DF.Logic.Screen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Util;
using Windows.UI;

namespace DF.Logic.View
{
    public class GameView : BaseView
    {
        public IRenderer Renderer { get; set; }

        public Vector2 CanvasSize { get; private set; }

        public Game Game;

        Color borderColor = Colors.Blue;

        SmoothViewPort worldViewPort = new SmoothViewPort();

        public WorldView MainWorldView { get; private set; } = new MainWorldView();
        public WorldView MinimapView { get; private set; } = new MinimapView();

        private bool directScrollHolding = false;
        public bool DirectScrollHolding
        {
            get
            {
                return directScrollHolding;
            }
            set
            {
                directScrollHolding = value;
                if(!directScrollHolding)
                {
                    MainWorldView.IsDragging = false;
                    MinimapView.IsDragging = false;
                }
                HandleHoverEvent(CurrentPointerPosition);
            }
        }

        private bool indirectScrollHolding = false;
        public bool IndirectScrollHolding
        {
            get
            {
                return indirectScrollHolding;
            }
            set
            {
                indirectScrollHolding = value;
                if (!indirectScrollHolding)
                {
                    MainWorldView.IsDragging = false;
                    MinimapView.IsDragging = false;
                }
                HandleHoverEvent(CurrentPointerPosition);
            }
        }

        public Vector2 IndirectScrollStartPosition;

        public Vector2 CurrentPointerPosition;

        public bool UsingMousePointer;

        public GameView()
        {
            MainWorldView.GameView = this;

            MainWorldView.ViewPort = worldViewPort;
            MainWorldView.ViewPort.SpaceOffset = WorldView.CellSize * World.ChunkSize * 0.5f;
            MainWorldView.ViewPort.SpaceZoom = 1.0f;

            MinimapView.GameView = this;
            MinimapView.ViewPort = new ViewPort
            {
                SpaceOffset = worldViewPort.SpaceOffset,
                SpaceZoom = 0.025f,
                Size = new IntVector2(200, 200),
            };
        }

        public void PrepareToDraw(Vector2 canvasSize)
        {
            CanvasSize = canvasSize;

            worldViewPort.Location = new IntVector2(0, 0);
            worldViewPort.Size = new IntVector2(canvasSize) - worldViewPort.Location * 2;

            var minimapMargin = 10;
            MinimapView.ViewPort.Location = new IntVector2((int)CanvasSize.X - minimapMargin - MinimapView.ViewPort.Size.X, minimapMargin);
        }

        public void Draw()
        {
            //DrawingSession.DrawRectangle(2, 2, CanvasSize.X-4, CanvasSize.Y-4, borderColor, 1);
            //DrawingSession.DrawRectangle(0, 0, CanvasSize.X-1, CanvasSize.Y-1, borderColor, 1);

            MainWorldView.Draw();
            MinimapView.Draw();

            var inderectScrollDelta = CurrentPointerPosition - IndirectScrollStartPosition;

            if(MainWorldView.IsDragging && IndirectScrollHolding && inderectScrollDelta.LengthSquared() != 0)
            {
                var scrollArrow = Game.Resources.Bitmaps.Hud.ScrollArrow;
                var rotation = (float)Math.Atan2(inderectScrollDelta.Y, inderectScrollDelta.X);
                var arrowSize = scrollArrow.SizeInPixels.Vector2;

                var scrollArrowStart = Game.Resources.Bitmaps.Hud.ScrollArrowStart;
                var startSize = scrollArrowStart.SizeInPixels.Vector2;

                Renderer.DrawBitmap(scrollArrowStart, IndirectScrollStartPosition - startSize * 0.5f, startSize);

                Renderer.DrawBitmap(scrollArrow, CurrentPointerPosition - arrowSize * 0.5f, arrowSize, rotation);
                
                /*
                Renderer.BeginSpriteBatch();
                var index = 0;
                Renderer.DrawBitmapFromSpriteSheetBatch(
                    Game.Resources.Bitmaps.TreesTest, 
                    index, 
                    CurrentPointerPosition, 
                    Game.Resources.Bitmaps.TreesTest.Regions[index].SourceRect.Size() * MainWorldView.ViewPort.SpaceZoom
                );
                Renderer.EndSpriteBatch();
                */
            }
        }

        public override void Update(TimeSpan elapsedTime)
        {
            if (UsingMousePointer)
            {
                HandleHoverEvent(CurrentPointerPosition);
            }
            worldViewPort.Update(elapsedTime);
            MainWorldView.Update(elapsedTime);
            MinimapView.Update(elapsedTime);
        }

        public void ProcessNewPointerPosition(Vector2 position)
        {
            HandleHoverEvent(position);
        }

        public void ProcessWheelDelta(int wheelDelta)
        {
            worldViewPort.AccelerateSpaceZoom(wheelDelta);
        }

        public override bool HandleHoverEvent(Vector2 pointerPosition)
        {
            UsingMousePointer = true;
            MinimapView.IsHovored = false;
            MainWorldView.IsHovored = false;
            var handledByMinimap = !MainWorldView.IsDragging && MinimapView.HandleHoverEvent(pointerPosition);
            if (!handledByMinimap)
            {
                var handledByMainWorldView = !MinimapView.IsDragging && MainWorldView.HandleHoverEvent(pointerPosition);
            }
            CurrentPointerPosition = pointerPosition;
            return true;
        }

        public override bool HandleLeftClick(Vector2 pointerPosition)
        {
            var handledByMinimap = MinimapView.HandleLeftClick(pointerPosition);
            if(!handledByMinimap)
            {
                var handledByMainWorldView = MainWorldView.HandleLeftClick(pointerPosition);
            }
            return true;
        }
    }
}
