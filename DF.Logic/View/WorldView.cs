﻿using DF.Logic.Entities;
using DF.Logic.Entities.Components;
using DF.Logic.Entities.EntityAction;
using DF.Logic.Screen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Util;
using Windows.UI;

namespace DF.Logic.View
{
    public abstract class WorldView : BaseView
    {
        public ViewPort ViewPort;
        public GameView GameView;

        Color gridColor = Colors.LightGray;
        Color bgColor = Colors.Black;

        protected bool drawGrid = false;
        protected bool drawVoronoiPoints = false;

        public static readonly IntVector2 CellSize = new IntVector2(64, 64);

        protected Vector2 viewPortLocation;
        protected Vector2 viewPortSize;
        protected Vector2 viewPortHalfSize;
        protected Vector2 viewPortCenter;

        protected Vector2 zoomedSpaceOffset;
        protected Vector2 vpcMinusZso;
        protected Vector2 zoomedCellSize;
        protected Vector2 zoomedCellSizeHalf;

        protected float destructionSelectionAnimationTime = 0;
        protected float destructionSelectionAnimationInterval = 1.5f;

        void PrepareToDraw()
        {
            viewPortLocation = ViewPort.Location.Vector2;
            viewPortSize = ViewPort.Size.Vector2;
            viewPortHalfSize = ViewPort.Size * 0.5f;
            viewPortCenter = viewPortLocation + viewPortHalfSize;

            var zoom = ViewPort.SpaceZoom;
            zoomedSpaceOffset = new IntVector2(ViewPort.SpaceOffset * zoom, v => (int)Math.Floor(v)).Vector2;
            zoomedCellSize = CellSize * zoom;
            zoomedCellSizeHalf = zoomedCellSize * 0.5f;

            vpcMinusZso = viewPortCenter - zoomedSpaceOffset;

            var pointerPosition = GameView.CurrentPointerPosition;
        }

        public void Draw()
        {
            PrepareToDraw();

            var renderer = GameView.Renderer;

            var startCell = CellFromScreen(viewPortLocation - Vector2.One);
            var endCell = CellFromScreen(viewPortLocation + viewPortSize + Vector2.One);
            var cellRange = endCell - startCell + IntVector2.One;

            renderer.BeginClippingRect(ViewPort.Location, ViewPort.Size);
            {
                renderer.FillRectangle(viewPortLocation, viewPortSize, bgColor);

                DrawWorldCells(startCell, cellRange);

                if (drawGrid)
                {
                    DrawGrid(startCell, endCell);
                }

                if (drawVoronoiPoints)
                {
                    DrawVoronoiPoints();
                }

                DrawEntities(startCell, cellRange);

                DrawMainWorldViewFrame();

                DrawHoveredCell();

                DrawBorder();
            }
            renderer.EndClippingRect();
        }

        protected abstract void DrawBorder();

        protected abstract void DrawMainWorldViewFrame();

        protected virtual void DrawWorldCells(IntVector2 startCell, IntVector2 cellRange)
        {
            var zoomedCellSize1 = zoomedCellSize + Vector2.One;

            GameView.Renderer.BeginSpriteBatch();

            foreach (var cellLocation in Grid.CoordinatesInRange(startCell, cellRange))
            {
                var p = vpcMinusZso + cellLocation * zoomedCellSize;

                var cell = GameView.Game.World.GetCell(cellLocation);
                var ground = cell?.Ground ?? Ground.None;

                if (cell.HasValue)
                {
                    var variant = cell.Value.GroundVariant;
                    var tile = GameView.Game.Resources.Bitmaps.Tiles.WorldGround[ground][variant];
                    if (ground == Ground.Sand || ground == Ground.Earth)
                    {
                        GameView.Renderer.DrawBitmapFromSpriteBatch(tile, p, zoomedCellSize1);
                    }
                }
                else if(IsDragging)
                {
                    var chunkCoordinates = GameView.Game.World.ChunkFromCell(cellLocation);
                    GameView.Game.World.Generator.GenerateCellChunk(chunkCoordinates);
                }
            }

            GameView.Renderer.EndSpriteBatch();
        }

        protected virtual void DrawEntities(IntVector2 startCell, IntVector2 cellRange)
        {
            var world = GameView.Game.World;

            GameView.Renderer.BeginSpriteBatch();

            //entity shadows:
            foreach (var cellLocation in Grid.CoordinatesInRange(startCell - IntVector2.One * 2, cellRange + IntVector2.One * 4))
            {
                var entities = world.Entities[cellLocation];

                if (entities != null)
                {
                    var p = vpcMinusZso + cellLocation * zoomedCellSize + zoomedCellSizeHalf;

                    foreach (var entity in entities)
                    {
                        switch (entity)
                        {
                            case WorkerBotEntity workerBot:
                                {
                                    var offset = workerBot.RealOffset * zoomedCellSizeHalf;
                                    var shadowBitmap = GameView.Game.Resources.Bitmaps.Effects.Shadow;
                                    var shadowRenderSize = shadowBitmap.SizeInPixels.Vector2 * ViewPort.SpaceZoom;
                                    GameView.Renderer.DrawBitmapFromSpriteBatch(shadowBitmap, p + offset, shadowRenderSize);
                                }
                                break;
                        }
                    }
                }
            }

            //entities:
            foreach (var cellLocation in Grid.CoordinatesInRange(startCell - IntVector2.One * 2, cellRange + IntVector2.One * 4))
            {
                var entities = world.Entities[cellLocation];

                if (entities != null)
                {
                    var p = vpcMinusZso + cellLocation * zoomedCellSize + zoomedCellSizeHalf;

                    foreach (var entity in entities)
                    {
                        switch (entity)
                        {
                            case BuildingEntity buildingEntity:
                                {
                                    if(buildingEntity.BuildingType == GameView.Game.BuildingTypes.Inserter)
                                    {
                                        { //base
                                            var bitmap = GameView.Game.Resources.Bitmaps.Entities.Buildings.ItemGrabberBase;
                                            var offset = buildingEntity.RealOffset * zoomedCellSizeHalf;
                                            var renderOffset = new Vector2(-32, -32) * ViewPort.SpaceZoom;
                                            var renderSize = bitmap.SizeInPixels.Vector2 * ViewPort.SpaceZoom * 1.0f;
                                            GameView.Renderer.DrawBitmapFromSpriteBatch(bitmap, p + offset + renderOffset, renderSize);
                                        }

                                        { //hand
                                            var bitmap = GameView.Game.Resources.Bitmaps.Entities.Buildings.ItemGrabberHand;
                                            var offset = buildingEntity.RealOffset * zoomedCellSizeHalf;
                                            offset += buildingEntity.ItemGrabber.HandLocation * zoomedCellSize;
                                            var renderOffset = new Vector2(-32, -32) * ViewPort.SpaceZoom;
                                            var renderSize = bitmap.SizeInPixels.Vector2 * ViewPort.SpaceZoom * 1.0f;
                                            GameView.Renderer.DrawBitmapFromSpriteBatch(bitmap, p + offset + renderOffset, renderSize);
                                        }
                                    }
                                    else
                                    {
                                        var bitmap = GameView.Game.Resources.Bitmaps.Entities.Buildings.Container;
                                        var offset = buildingEntity.RealOffset * zoomedCellSizeHalf;
                                        var renderOffset = new Vector2(-19, -27) * ViewPort.SpaceZoom;
                                        var renderSize = bitmap.SizeInPixels.Vector2 * ViewPort.SpaceZoom * 1.3f;
                                        GameView.Renderer.DrawBitmapFromSpriteBatch(bitmap, p + offset + renderOffset, renderSize);
                                    }
                                }
                                break;
                            case RefinableEntity refinableEntity when refinableEntity.RefinableType == GameView.Game.RefinableTypes.Tree:
                                {
                                    var variant = refinableEntity.Variant;
                                    var sheet = GameView.Game.Resources.Bitmaps.TreesTest;
                                    var offset = refinableEntity.RealOffset * zoomedCellSizeHalf + zoomedCellSizeHalf * 0.5f;
                                    var tintValue = (float)Math.Sin((destructionSelectionAnimationTime / destructionSelectionAnimationInterval) * Math.PI * 2) * 0.5f + 0.5f;
                                    var tint = Vector4.One;
                                    if(GameView.Game.World.EntitiesMarkedForDestruction.KeysContainingValue(refinableEntity).Any())
                                    {
                                        tint = new Vector4(1, 1 - tintValue * 0.5f, 1 - tintValue * 0.5f, 1);
                                    }
                                    GameView.Renderer.DrawBitmapFromSpriteSheetBatch(sheet, variant, p + offset, sheet.Regions[variant].Size() * ViewPort.SpaceZoom/* * 0.7f*/, tint);
                                }
                                break;
                            case WorkerBotEntity workerBot:
                                {
                                    var bitmap = GameView.Game.Resources.Bitmaps.Entities.WorkerBot;
                                    var offset = workerBot.RealOffset * zoomedCellSizeHalf;
                                    var dummyLevitationAnimationOffset = ((float)Math.Sin((destructionSelectionAnimationTime / destructionSelectionAnimationInterval) * Math.PI * 2) * 0.5f + 0.5f) * ViewPort.SpaceZoom * 2.5f;
                                    var elevationOffset = zoomedCellSizeHalf.Y + dummyLevitationAnimationOffset;
                                    var renderSize = bitmap.SizeInPixels.Vector2 * ViewPort.SpaceZoom;
                                    
                                    {
                                        GameView.Renderer.DrawBitmapFromSpriteBatch(bitmap, p + offset + new Vector2(0, -elevationOffset), renderSize);
                                    }

                                    if(workerBot.IsAttacking && workerBot.PlannedAction is AttackEntityAction attackEntityAction)
                                    {
                                        var attackEffect = GameView.Game.Resources.Bitmaps.Effects.Beam;
                                        var diff = (attackEntityAction.EntityToAttack.CellLocation - workerBot.CellLocation).Vector2 * CellSize + (attackEntityAction.EntityToAttack.RealOffset - workerBot.RealOffset) * CellSize * 0.5f;
                                        var rotationAngle = (float)Math.Atan2(diff.Y, diff.X);
                                        GameView.Renderer.DrawBitmapFromSpriteBatch(attackEffect, p + offset + new Vector2(0, -elevationOffset) + renderSize * 0.5f + new Vector2(0, -3) * ViewPort.SpaceZoom, attackEffect.SizeInPixels.Vector2 * ViewPort.SpaceZoom, rotationAngle, new Vector2(0, attackEffect.SizeInPixels.Y * 0.5f));
                                    }
                                }
                                break;
                            case ItemEntity itemEntity:
                                {
                                    var bitmap = GameView.Game.Resources.Bitmaps.Items.Wood;
                                    var offset = itemEntity.RealOffset * zoomedCellSizeHalf;
                                    //var bitmapCenterOffset = bitmap.SizeInPixels * (0.5f * ViewPort.SpaceZoom);
                                    GameView.Renderer.DrawBitmapFromSpriteBatch(bitmap, p + offset/* - bitmapCenterOffset*/, bitmap.SizeInPixels.Vector2 * ViewPort.SpaceZoom);
                                }
                                break;
                        }
                    }
                }
            }

            //markers:
            foreach (var cellLocation in Grid.CoordinatesInRange(startCell - IntVector2.One * 2, cellRange + IntVector2.One * 4))
            {
                var markers = world.ItemGrabberMarkedLocations[cellLocation];

                if (markers != null)
                {
                    var p = vpcMinusZso + cellLocation * zoomedCellSize + zoomedCellSizeHalf;

                    foreach (var marker in markers)
                    {
                        switch (marker)
                        {
                            case ItemGrabberInputMarker inputMarker:
                                {
                                    var offset = -zoomedCellSizeHalf;
                                    var bitmap = GameView.Game.Resources.Bitmaps.Markers.ItemGrabberInput;
                                    var renderSize = bitmap.SizeInPixels.Vector2 * ViewPort.SpaceZoom;
                                    GameView.Renderer.DrawBitmapFromSpriteBatch(bitmap, p + offset, renderSize);
                                }
                                break;
                            case ItemGrabberOutputMarker outputMarker:
                                {
                                    var offset = -zoomedCellSizeHalf;
                                    var bitmap = GameView.Game.Resources.Bitmaps.Markers.ItemGrabberOutput;
                                    var renderSize = bitmap.SizeInPixels.Vector2 * ViewPort.SpaceZoom;
                                    GameView.Renderer.DrawBitmapFromSpriteBatch(bitmap, p + offset, renderSize);
                                }
                                break;
                        }
                    }
                }
            }

            GameView.Renderer.EndSpriteBatch();
        }

        protected abstract void DrawHoveredCell();

        protected virtual void DrawGrid(IntVector2 startCell, IntVector2 endCell)
        {
            for (int x = startCell.X; x < endCell.X + 1; ++x)
            {
                var cellLocation = new IntVector2(x, startCell.Y);
                var cellLocation2 = new IntVector2(x, endCell.Y + 1);
                var p = vpcMinusZso + cellLocation * zoomedCellSize;
                var p2 = vpcMinusZso + cellLocation2 * zoomedCellSize;
                GameView.Renderer.DrawLine(p, p2, gridColor);
            }

            for (int y = startCell.Y; y < endCell.Y + 1; ++y)
            {
                var cellLocation = new IntVector2(startCell.X, y);
                var cellLocation2 = new IntVector2(endCell.X + 1, y);
                var p = vpcMinusZso + cellLocation * zoomedCellSize;
                var p2 = vpcMinusZso + cellLocation2 * zoomedCellSize;
                GameView.Renderer.DrawLine(p, p2, gridColor);
            }
        }

        void DrawVoronoiPoints()
        {
            foreach (var point in GameView.Game.World.GetAllVoronoiPoints())
            {
                var cellLocation = point.cellCoordinates;
                Color color;
                switch (point.ground)
                {
                    case Ground.Earth: color = Colors.Red; break;
                    case Ground.Grass: color = Colors.Green; break;
                    case Ground.Sand: color = Colors.Yellow; break;
                }
                var p = vpcMinusZso + cellLocation * zoomedCellSize;
                GameView.Renderer.FillRectangle(p + Vector2.One, zoomedCellSize - Vector2.One, color);
            }
        }

        protected Vector2 ScreenFromWorld(Vector2 worldCoordinates)
        {
            return (ViewPort.Location + ViewPort.Size * 0.5f) + worldCoordinates * ViewPort.SpaceZoom - ViewPort.SpaceOffset * ViewPort.SpaceZoom; //TODO: optimize
        }

        protected Vector2 WorldFromScreen(Vector2 screenCoordinates)
        {
            return (screenCoordinates - vpcMinusZso) / ViewPort.SpaceZoom;
        }

        protected IntVector2 CellFromWorld(Vector2 worldCoordinates)
        {
            return new IntVector2(worldCoordinates / CellSize, x => (int)Math.Floor(x));
        }

        protected IntVector2 CellFromScreen(Vector2 screenCoordinates)
        {
            var worldCoordinate = WorldFromScreen(screenCoordinates);
            return CellFromWorld(worldCoordinate);
        }

        public (Vector2 topLeft, Vector2 bottomDown) VisualWorldBounds()
        {
            return (WorldFromScreen(ViewPort.Location.Vector2), WorldFromScreen((ViewPort.Location + ViewPort.Size).Vector2));
        }
    }
}
