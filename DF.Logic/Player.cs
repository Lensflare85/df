﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic
{
    public sealed class Player
    {
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            return obj.GetType() == typeof(Player) && Name == ((Player)obj).Name;
        }

        public override int GetHashCode()
        {
            return Name?.GetHashCode() ?? 0;
        }
    }
}
