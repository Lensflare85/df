﻿namespace DF.Logic.Inventory
{
    public interface IInventory<T>
    {
        int Add(T itemType, int numberOfItems);

        int CanBeAddedCount(T itemType);

        bool CanBeAddedAny(T itemType);

        int Count(T itemType);

        bool Contains(T itemType);

        int Remove(T itemType, int numberOfItems);

        int Remove(T itemType);

        int TransferTo(IInventory<T> targetInventory);

        int TransferTo(IInventory<T> targetInventory, T itemType);

        bool CanTransferAny(IInventory<T> targetInventory);

        bool CanTransferAny(IInventory<T> targetInventory, T itemType);

        T[] DistinctContents();

        bool IsEmpty();

        bool IsFull();
    }
}
