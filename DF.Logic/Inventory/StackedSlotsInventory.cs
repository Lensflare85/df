﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Inventory
{
    public abstract class StackedSlotsInventory<TItem> : Inventory<TItem, InventorySlotStack<TItem>> where TItem : class
    {
        public StackedSlotsInventory(int numberOfSlots) : base(numberOfSlots)
        {
        }

        protected abstract int MaxStackSize(TItem itemType);

        public override bool IsFull()
        {
            foreach (var slot in slots)
            {
                if (slot == null || slot.NumberOfItems < MaxStackSize(slot.ItemType))
                {
                    return false;
                }
            }
            return true;
        }

        public override int Add(TItem itemType, int numberOfItems)
        {
            Debug.Assert(numberOfItems > 0);

            int maxStackSize = MaxStackSize(itemType);
            var remainingToAdd = numberOfItems;
            var added = 0;

            for (int i = 0; i < NumberOfSlots; ++i)
            {
                var slot = slots[i];
                if (slot != null && slot.ItemType == itemType && slot.NumberOfItems < maxStackSize)
                {
                    var addable = maxStackSize - slot.NumberOfItems;
                    if (remainingToAdd <= addable)
                    {
                        slot.NumberOfItems += remainingToAdd;
                        added += remainingToAdd;
                        return added;
                    }
                    else
                    {
                        slot.NumberOfItems += addable;
                        added += addable;
                        remainingToAdd -= addable;
                    }
                }
            }

            for (int i = 0; i < NumberOfSlots; ++i)
            {
                var slot = slots[i];
                if (slot == null)
                {
                    if (remainingToAdd <= maxStackSize)
                    {
                        slots[i] = new InventorySlotStack<TItem>(itemType, remainingToAdd);
                        added += remainingToAdd;
                        return added;
                    }
                    else
                    {
                        slots[i] = new InventorySlotStack<TItem>(itemType, maxStackSize);
                        added += maxStackSize;
                        remainingToAdd -= maxStackSize;
                    }
                }
            }

            if(added > 0)
            {
                HandleAdded();
            }

            return added;
        }

        public override int CanBeAddedCount(TItem itemType)
        {
            int maxStackSize = MaxStackSize(itemType);
            var canBeAdded = 0;

            for (int i = 0; i < NumberOfSlots; ++i)
            {
                var slot = slots[i];
                if (slot == null)
                {
                    canBeAdded += maxStackSize;
                }
                else if (slot != null && slot.ItemType == itemType && slot.NumberOfItems < maxStackSize)
                {
                    var addable = maxStackSize - slot.NumberOfItems;
                    canBeAdded += addable;
                }
            }

            return canBeAdded;
        }

        public override bool CanBeAddedAny(TItem itemType)
        {
            int maxStackSize = MaxStackSize(itemType);

            for (int i = 0; i < NumberOfSlots; ++i)
            {
                var slot = slots[i];
                if (slot == null || slot.ItemType == itemType && slot.NumberOfItems < maxStackSize)
                {
                    return true;
                }
            }

            return false;
        }

        public override int Count(TItem itemType)
        {
            var numberOfItems = 0;

            for (int i = 0; i < NumberOfSlots; ++i)
            {
                var slot = slots[i];
                if (slot != null && slot.ItemType == itemType)
                {
                    numberOfItems += slot.NumberOfItems;
                }
            }

            return numberOfItems;
        }

        public override bool Contains(TItem itemType)
        {
            for (int i = 0; i < NumberOfSlots; ++i)
            {
                var slot = slots[i];
                if (slot != null && slot.ItemType == itemType)
                {
                    return true;
                }
            }

            return false;
        }

        public override int Remove(TItem itemType, int numberOfItems)
        {
            var removed = 0;
            var remainingToRemove = numberOfItems;

            for (int i = NumberOfSlots - 1; i >= 0; --i)
            {
                var slot = slots[i];
                if (slot != null && slot.ItemType == itemType)
                {
                    if (remainingToRemove <= slot.NumberOfItems)
                    {
                        slot.NumberOfItems -= remainingToRemove;
                        if (slot.NumberOfItems < 1)
                        {
                            slots[i] = null;
                        }
                        removed += remainingToRemove;
                        return removed;
                    }
                    else
                    {
                        removed += slot.NumberOfItems;
                        remainingToRemove -= slot.NumberOfItems;
                        slots[i] = null;
                    }
                }
            }

            if(removed > 0)
            {
                HandleRemoved();
            }

            return removed;
        }

        public override int Remove(TItem itemType)
        {
            var removed = 0;

            for (int i = NumberOfSlots - 1; i >= 0; --i)
            {
                var slot = slots[i];
                if (slot != null && slot.ItemType == itemType)
                {
                    removed += slot.NumberOfItems;
                    slots[i] = null;
                }
            }

            if (removed > 0)
            {
                HandleRemoved();
            }

            return removed;
        }

        public override TItem[] DistinctContents()
        {
            var itemTypes = new List<TItem>();

            foreach (var slot in slots)
            {
                if (slot != null && !itemTypes.Contains(slot.ItemType))
                {
                    itemTypes.Add(slot.ItemType);
                }
            }

            return itemTypes.ToArray();
        }

        protected bool RemoveOneFromSlot(int slotIndex)
        {
            var slot = slots[slotIndex];
            if (slot == null)
            {
                return false;
            }

            slot.NumberOfItems -= 1;
            if (slot.NumberOfItems == 0)
            {
                slots[slotIndex] = null;
            }
            HandleRemoved();
            return true;
        }
    }

    public class InventorySlotStack<T>
    {
        public T ItemType;
        public int NumberOfItems;

        public InventorySlotStack(T itemType, int numberOfItems)
        {
            ItemType = itemType;
            NumberOfItems = numberOfItems;
        }

        public override string ToString()
        {
            return $"{NumberOfItems} {ItemType}";
        }
    }
}
