﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Inventory
{
    public abstract class Inventory<TItem,TSlot> : IInventory<TItem>
    {
        protected TSlot[] slots;
        public int NumberOfSlots => slots.Length;

        public Inventory(int numberOfSlots)
        {
            slots = new TSlot[numberOfSlots];
        }

        public TSlot this[int slotIndex]
        {
            get => slots[slotIndex];
            set
            {
                var oldSlot = slots[slotIndex];
                slots[slotIndex] = value;

                if(oldSlot != null)
                {
                    HandleRemoved();
                }
                if(value != null)
                {
                    HandleAdded();
                }
            }
        }

        public bool IsEmpty()
        {
            foreach(var slot in slots)
            {
                if (slot != null) return false;
            }
            return true;
        }

        public bool HasEmptySlots()
        {
            foreach (var slot in slots)
            {
                if (slot == null) return true;
            }
            return false;
        }

        public int? FirstEmptySlotIndex()
        {
            for (int i=0; i<slots.Length; ++i)
            {
                if(slots[i] == null)
                {
                    return i;
                }
            }
            return null;
        }
        
        public int TransferTo(IInventory<TItem> targetInventory)
        {
            var transferred = 0;
            var itemTypes = DistinctContents();
            foreach(var itemType in itemTypes)
            {
                transferred += TransferTo(targetInventory, itemType);
            }
            return transferred;
        }

        public int TransferTo(IInventory<TItem> targetInventory, TItem itemType)
        {
            var removed = Remove(itemType);
            var transferred = targetInventory.Add(itemType, removed);
            var toPutBack = removed - transferred;
            if (toPutBack > 0)
            {
                Add(itemType, toPutBack);
            }
            return transferred;
        }

        public bool CanTransferAny(IInventory<TItem> targetInventory)
        {
            foreach(var itemType in DistinctContents())
            {
                if(targetInventory.CanBeAddedAny(itemType))
                {
                    return true;
                }
            }

            return false;
        }

        public bool CanTransferAny(IInventory<TItem> targetInventory, TItem itemType)
        {
            if(!Contains(itemType))
            {
                return false;
            }

            return targetInventory.CanBeAddedAny(itemType);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Inventory Slots:");
            foreach(var slot in slots)
            {
                sb.AppendLine($"[{slot?.ToString() ?? ""}]");
            }
            return sb.ToString();
        }

        public abstract TItem[] DistinctContents();
        public abstract int Count(TItem itemType);
        public abstract bool Contains(TItem itemType);
        public abstract int CanBeAddedCount(TItem itemType);
        public abstract bool CanBeAddedAny(TItem itemType);
        public abstract int Add(TItem itemType, int numberOfItems);
        public abstract int Remove(TItem itemType, int numberOfItems);
        public abstract int Remove(TItem itemType);
        public abstract bool IsFull();

        protected abstract void HandleAdded();
        protected abstract void HandleRemoved();
    }
}
