﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Inventory
{
    public abstract class SingleSlotsInventory<TItem> : Inventory<TItem, TItem> where TItem : class
    {
        public SingleSlotsInventory(int numberOfSlots) : base(numberOfSlots)
        {
        }

        public override bool IsFull()
        {
            foreach (var slot in slots)
            {
                if (slot == null) return false;
            }
            return true;
        }

        public override int Add(TItem itemType, int numberOfItems)
        {
            Debug.Assert(numberOfItems > 0);

            if(numberOfItems <= 0)
            {
                return 0;
            }

            var remainingToAdd = numberOfItems;
            var added = 0;

            for (int i = 0; i < NumberOfSlots; ++i)
            {
                if (slots[i] == null)
                {
                    slots[i] = itemType;
                    added += 1;
                    remainingToAdd -= 1;
                    if(remainingToAdd <= 0)
                    {
                        break;
                    }
                }
            }

            if (added > 0)
            {
                HandleAdded();
            }

            return added;
        }

        public override int CanBeAddedCount(TItem itemType)
        {
            var canBeAdded = 0;

            for (int i = 0; i < NumberOfSlots; ++i)
            {
                if (slots[i] == null)
                {
                    canBeAdded += 1;
                }
            }

            return canBeAdded;
        }

        public override bool CanBeAddedAny(TItem itemType)
        {
            for (int i = 0; i < NumberOfSlots; ++i)
            {
                if (slots[i] == null)
                {
                    return true;
                }
            }

            return false;
        }

        public override int Count(TItem itemType)
        {
            var numberOfItems = 0;

            for (int i = 0; i < NumberOfSlots; ++i)
            {
                var slot = slots[i];
                if (slot != null && slot == itemType)
                {
                    numberOfItems += 1;
                }
            }

            return numberOfItems;
        }

        public override bool Contains(TItem itemType)
        {
            for (int i = 0; i < NumberOfSlots; ++i)
            {
                var slot = slots[i];
                if (slot != null && slot == itemType)
                {
                    return true;
                }
            }
            return false;
        }

        public override int Remove(TItem itemType, int numberOfItems)
        {
            Debug.Assert(numberOfItems > 0);

            if (numberOfItems <= 0)
            {
                return 0;
            }

            var removed = 0;
            var remainingToRemove = numberOfItems;

            for (int i = NumberOfSlots - 1; i >= 0; --i)
            {
                var slot = slots[i];
                if (slot != null && slot == itemType)
                {
                    slots[i] = null;
                    removed += 1;
                    remainingToRemove -= 1;
                    if(remainingToRemove <= 0)
                    {
                        break;
                    }
                }
            }

            if(removed > 0)
            {
                HandleRemoved();
            }

            return removed;
        }

        public override int Remove(TItem itemType)
        {
            var removed = 0;

            for (int i = NumberOfSlots - 1; i >= 0; --i)
            {
                var slot = slots[i];
                if (slot != null && slot == itemType)
                {
                    slots[i] = null;
                    removed += 1;
                }
            }

            if (removed > 0)
            {
                HandleRemoved();
            }

            return removed;
        }

        public override TItem[] DistinctContents()
        {
            var itemTypes = new List<TItem>();

            foreach (var slot in slots)
            {
                if (slot != null && !itemTypes.Contains(slot))
                {
                    itemTypes.Add(slot);
                }
            }

            return itemTypes.ToArray();
        }

        protected bool RemoveOneFromSlot(int slotIndex)
        {
            var slot = slots[slotIndex];
            if (slot == null)
            {
                return false;
            }
            else
            {
                slots[slotIndex] = null;
                HandleRemoved();
                return true;
            }
        }
    }
}
