﻿using DF.Logic.Entities;
using DF.Logic.Entities.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;
using Util.Map;
using Util.Random;
using Windows.UI;

namespace DF.Logic
{
    public sealed class WorldGenerator
    {
        public World World { get; set; }

        Ground[] possibleGrounds = new[] { Ground.Earth, Ground.Sand, Ground.Sand };

        public void GenerateCellChunk(IntVector2 chunkCoordinates)
        {
            const int chunkSalt = 171945928;
            var chunkSeed = chunkCoordinates.GetHashCode() ^ chunkSalt ^ World.GlobalSeed;
            var rng = new RandomNumberGenerator(chunkSeed);

            var cellChunkSize = World.CellChunks.ChunkSize;
            var chunk = new Map2<WorldCell>(cellChunkSize);

            var voronoiPointsIncludingNeighbors = new Dictionary<IntVector2, Ground>();

            foreach (var chunkDiff in Grid.CoordinatesInRange(-IntVector2.One, new IntVector2(3, 3)))
            { //TODO: make extra function for this case
                var vc = chunkCoordinates + chunkDiff;

                var points = World.VoronoiPointsChunks[vc];
                if (points == null)
                {
                    points = GenerateVoronoiPointsForChunk(vc);
                }

                foreach (var point in points)
                {
                    voronoiPointsIncludingNeighbors[point.Key] = point.Value;
                }
            }

            foreach (var coordinate in Grid.CoordinatesInSize(cellChunkSize))
            {
                var absCoord = coordinate + chunkCoordinates * cellChunkSize;
                //var points = voronoiPointsIncludingNeighbors.Where(p => p.Key.DistanceTo(absCoord) < 2 * ChunkSize.X);
                var points = voronoiPointsIncludingNeighbors;
                var pointsFound = points.Any();
                var min = points.OrderBy(p => p.Key.DistanceSquaredTo(absCoord)).FirstOrDefault();
                var ground = min.Value;

                if (ground == Ground.Earth)
                {
                    var distanceToVoronoidEarth = min.Key.DistanceTo(absCoord);
                    int treePercentChance = (int)(1f / distanceToVoronoidEarth * 100f);
                    var shouldPlaceTree = rng.PercentChance(treePercentChance);
                    if (shouldPlaceTree)
                    {
                        var variant = rng.Next(World.Game.Resources.Bitmaps.TreesTest.Regions.Length);
                        //var offset = new IntVector2(rng.Next(WorldEntity.OffsetRange), rng.Next(WorldEntity.OffsetRange));
                        var offset = new IntVector2(rng.Next(WorldEntity.OffsetRange), rng.Next(WorldEntity.OffsetRange)) - new IntVector2(WorldEntity.OffsetRange / 2, WorldEntity.OffsetRange / 2);
                        var tree = new RefinableEntity(World.Game.RefinableTypes.Tree)
                        {
                            Game = World.Game,
                            Variant = variant,
                            Offset = offset,
                            CellLocation = absCoord,
                        };
                        tree.HitPoints = new HitPoints(tree) { MaxHitPoints = 100, CurrentHitPoints = 100 };
                        World.AddEntity(tree);
                    }
                }

                var numberOfVariations = World.Game.Resources.Bitmaps.Tiles.WorldGround.GetOrDefault(ground)?.NumberOfVariations ?? 0;

                chunk[coordinate] = new WorldCell()
                {
                    Ground = pointsFound ? ground : Ground.None,
                    Wall = ground == Ground.Earth ? Wall.Earth : Wall.None,
                    GroundVariant = rng.Next(numberOfVariations),
                };
            }

            World.CellChunks[chunkCoordinates] = chunk;

            var colors = chunk.AllValues().Select(v => v.Ground == Ground.Sand ? Colors.NavajoWhite : Colors.SaddleBrown).ToArray();
            var bitmap = World.Game.ResourceCreator.CreateBitmapFromColors(colors, cellChunkSize);

            World.ChunkBitmaps[chunkCoordinates] = bitmap;
            //debug print elapsed time
        }

        Dictionary<IntVector2, Ground> GenerateVoronoiPointsForChunk(IntVector2 chunk)
        {
            const int voronoiChunkSalt = 326907365;
            var chunkSeed = chunk.GetHashCode() ^ voronoiChunkSalt ^ World.GlobalSeed;
            var rng = new RandomNumberGenerator(chunkSeed);
            var randomPicker = new RandomPicker(rng);

            var voronoiPoints = new Dictionary<IntVector2, Ground>();

            var numberOfVoronoiPoints = World.ChunkSize.X * World.ChunkSize.Y / 100;
            if (numberOfVoronoiPoints == 0) numberOfVoronoiPoints = 1;
            for (int i = 0; i < numberOfVoronoiPoints; ++i)
            {
                var point = new IntVector2(rng.Next(World.ChunkSize.X), rng.Next(World.ChunkSize.Y)) + chunk * World.ChunkSize;
                voronoiPoints[point] = randomPicker.PickFrom(possibleGrounds);
            }

            World.VoronoiPointsChunks[chunk] = voronoiPoints;

            return voronoiPoints;
        }
    }
}
