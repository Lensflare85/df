﻿using DF.Logic.Entities.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Entities
{
    public class ItemEntity : WorldEntity
    {
        public readonly ItemType ItemType;
        public MobileEntity TargetedByEntity;

        public ItemEntity(ItemType itemType) : base()
        {
            Passable = true;
            ItemType = itemType;
        }
    }
}
