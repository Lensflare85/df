﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Entities.Components
{
    public interface IWorldEntityComponent
    {
        WorldEntity WorldEntity { get; set; }
    }
}
