﻿using System;
using System.Diagnostics;
using System.Numerics;
using Util;

namespace DF.Logic.Entities.Components
{
    public sealed class ItemGrabber : IWorldEntityComponent
    {
        private const float angleEpsilonDegrees = 1;
        private const float angleEpsilon = (float)Math.PI * angleEpsilonDegrees / 180;

        private const float armAngularVelocityDegreesPerSecond = 70;
        private const float armAngularVelocityRadiansPerSecond = (float)(Math.PI) * armAngularVelocityDegreesPerSecond / 180;

        public WorldEntity WorldEntity { get; set; }

        public ItemGrabberInputMarker InputMarker;
        public ItemGrabberOutputMarker OutputMarker;

        public Vector2 HandLocation { get; private set; } = -Vector2.UnitX; //TODO: should be Quantized

        private bool outputMode = true;

        public void Update(TimeSpan elapsedTime)
        {
            var markerLocation = outputMode ? OutputMarker.Location : InputMarker.Location;
            var vMarker = (markerLocation - WorldEntity.CellLocation).Vector2;
            var vHand = HandLocation;

            var angleDelta = vHand.AngleDeltaTo(vMarker);
            var angleDeltaAbs = Math.Abs(angleDelta);
            if (angleDeltaAbs > angleEpsilon)
            {
                var sign = Math.Sign(angleDelta);

                var aStep = (float)elapsedTime.TotalSeconds * armAngularVelocityRadiansPerSecond;
                if (aStep > angleDeltaAbs)
                {
                    aStep = angleDeltaAbs;
                }
                HandLocation = HandLocation.RotatedBy(aStep * sign);
            }
            else
            {
                outputMode = !outputMode;
            }
        }
    }

    public abstract class ItemGrabberMarker
    {
        public ItemGrabber ItemGrabber { get; protected set; }
        public IntVector2 Location { get; protected set; }

        public ItemGrabberMarker(ItemGrabber itemGrabber, IntVector2 location)
        {
            ItemGrabber = itemGrabber;
            Location = location;
        }
    }

    public sealed class ItemGrabberInputMarker : ItemGrabberMarker
    {
        public ItemGrabberInputMarker(ItemGrabber itemGrabber, IntVector2 location) : base(itemGrabber, location)
        {
        }
    }

    public sealed class ItemGrabberOutputMarker : ItemGrabberMarker
    {
        public ItemGrabberOutputMarker(ItemGrabber itemGrabber, IntVector2 location) : base(itemGrabber, location)
        {
        }
    }
}
