﻿using DF.Logic.Entities.Types;
using DF.Logic.Inventory;

namespace DF.Logic.Entities.Components
{
    public interface IItemsInventory : IInventory<ItemType>, IWorldEntityComponent
    {
        
    }
}
