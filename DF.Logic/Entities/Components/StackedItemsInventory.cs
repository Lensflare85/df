﻿using DF.Logic.Entities.Types;
using System.Collections.Generic;
using System.Diagnostics;
using System;
using DF.Logic.Inventory;

namespace DF.Logic.Entities.Components
{
    public sealed class StackedItemsInventory : StackedSlotsInventory<ItemType>, IItemsInventory
    {
        public WorldEntity WorldEntity { get; set; }

        public StackedItemsInventory(WorldEntity worldEntity, int numberOfSlots) : base(numberOfSlots)
        {
            WorldEntity = worldEntity;
        }

        protected override int MaxStackSize(ItemType itemType)
        {
            return itemType.MaxInventoryStackSize;
        }

        protected override void HandleAdded()
        {
            //TODO: ...
        }

        protected override void HandleRemoved()
        {
            //TODO: ...
        }
    }

    public sealed class ItemStack : InventorySlotStack<ItemType>
    {
        public ItemStack(ItemType itemType, int numberOfItems) : base(itemType, numberOfItems)
        {
        }
    }
}
