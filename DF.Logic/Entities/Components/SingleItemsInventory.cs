﻿using DF.Logic.Entities.Types;
using DF.Logic.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Entities.Components
{
    public sealed class SingleItemsInventory : SingleSlotsInventory<ItemType>, IItemsInventory
    {
        public WorldEntity WorldEntity { get; set; }

        public SingleItemsInventory(WorldEntity worldEntity, int numberOfSlots) : base(numberOfSlots)
        {
            WorldEntity = worldEntity;
        }

        protected override void HandleAdded()
        {
            //TODO: ...
        }

        protected override void HandleRemoved()
        {
            //TODO: ...
        }
    }
}
