﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Entities.Components
{
    public sealed class HitPoints : IWorldEntityComponent
    {
        public WorldEntity WorldEntity { get; set; }

        public int CurrentHitPoints { get; set; }
        public int MaxHitPoints { get; set; }

        public HitPoints(WorldEntity worldEntity)
        {
            WorldEntity = worldEntity;
        }

        public void TakeDamageFrom(WorldEntity entity, int damagePoints)
        {
            if (CurrentHitPoints > 0)
            {
                CurrentHitPoints -= damagePoints;
                if (CurrentHitPoints <= 0)
                {
                    WorldEntity.Game.World.EntitiesToDestroy.Add(WorldEntity);
                }
            }
        }
    }
}
