﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Entities.Components
{
    public sealed class ActionTimer : IWorldEntityComponent, IDisposable
    {
        public WorldEntity WorldEntity { get; set; }

        private const int timePartsPerSecond = 1000;
        private readonly int interval;
        private int currentTime = 0;
        private Action action;

        public ActionTimer(WorldEntity worldEntity, float intervalInSeconds, Action action)
        {
            WorldEntity = worldEntity;
            interval = (int)(intervalInSeconds * timePartsPerSecond);
            this.action = action;
        }

        public void Dispose()
        {
            action = null;
        }

        public void ResetTimer()
        {
            currentTime = 0;
        }

        public void Update(TimeSpan elapsedTime)
        {
            currentTime += (int)(elapsedTime.TotalSeconds * timePartsPerSecond);

            if (currentTime >= interval)
            {
                action();
                currentTime -= interval;
            }
        }
    }
}
