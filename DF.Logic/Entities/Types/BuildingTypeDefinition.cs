﻿using Util;

namespace DF.Logic.Entities.Types
{
    public sealed class BuildingTypeDefinition
    {
        public readonly BuildingType Container = new BuildingType(
            cellRange: IntVector2.One, 
            maxHitpoints: 100, 
            numberOfInventorySlots: 1, 
            inventorySlotsAreStacked: true, 
            isItemGrabber: false
        );

        public readonly BuildingType Inserter = new BuildingType(
            cellRange: IntVector2.One, 
            maxHitpoints: 20,
            numberOfInventorySlots: 0,
            inventorySlotsAreStacked: false,
            isItemGrabber: true
        );
    }
}
