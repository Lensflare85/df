﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Entities.Types
{
    public sealed class ItemTypeDefinition
    {
        public readonly ItemType Dummy = new ItemType("Item_Dummy", maxInventoryStackSize: 10);
        public readonly ItemType Wood = new ItemType("Item_Wood", maxInventoryStackSize: 5);
    }
}
