﻿using Util;

namespace DF.Logic.Entities.Types
{
    public sealed class BuildingType
    {
        public readonly IntVector2 CellRange;
        public readonly int MaxHitpoints;
        public readonly int NumberOfInventorySlots;
        public readonly bool InventorySlotsAreStacked;
        public readonly bool IsItemGrabber;

        public BuildingType(IntVector2 cellRange, int maxHitpoints, int numberOfInventorySlots, bool inventorySlotsAreStacked, bool isItemGrabber)
        {
            CellRange = cellRange;
            MaxHitpoints = maxHitpoints;
            NumberOfInventorySlots = numberOfInventorySlots;
            InventorySlotsAreStacked = inventorySlotsAreStacked;
            IsItemGrabber = isItemGrabber;
        }
    }
}
