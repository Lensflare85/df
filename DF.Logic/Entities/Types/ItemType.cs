﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace DF.Logic.Entities.Types
{
    public sealed class ItemType
    {
        private readonly string NameKey;

        public readonly int MaxInventoryStackSize;

        public ItemType(string nameKey, int maxInventoryStackSize)
        {
            NameKey = nameKey;
            MaxInventoryStackSize = maxInventoryStackSize;
        }

        public string Name => names.GetOrDefault(NameKey) ?? defaultName;

        private const string defaultName = "?";
        private static readonly Dictionary<string, string> names = new Dictionary<string, string>() {
            { "Item_Dummy", "Dummy" },
            { "Item_Wood", "Wood" },
        };

        public override string ToString()
        {
            return Name;
        }
    }
}
