﻿namespace DF.Logic.Entities.Types
{
    public sealed class RefinableTypeDefinition
    {
        public readonly RefinableType Tree = new RefinableType();
    }
}
