﻿using DF.Logic.Entities.Components;
using System;
using System.Linq;
using Util;

namespace DF.Logic.Entities
{
    public sealed class WorkerBotEntity : MobileEntity
    {
        public Player Owner { get; set; }

        private ActionTimer planAssignmentTimer;

        public WorkerBotEntity(int seed) : base(seed)
        {
            Passable = true;

            HitPoints = new HitPoints(this) { MaxHitPoints = 100, CurrentHitPoints = 100 };

            Inventory = new SingleItemsInventory(this, numberOfSlots: 2);

            planAssignmentTimer = new ActionTimer(this, intervalInSeconds: 1, action: AssignNewPlan);
        }

        private void AssignNewPlan()
        {
            if(Inventory.IsFull())
            {
                var depositItems = FindContainerToDepositItemsAndAssignPlan();
                if(!depositItems)
                {
                    FindTreeToAttackAndAssignPlan();
                }
            }
            else
            {
                bool pickupItem = FindItemToPickupAndAssignPlan();
                if(!pickupItem)
                {
                    FindTreeToAttackAndAssignPlan();
                }
            }
        }

        public override void HandleNewCellLocation(IntVector2 newLocation)
        {
            base.HandleNewCellLocation(newLocation);
            CalculateNextOffsetTarget();
        }

        private bool FindItemToPickupAndAssignPlan()
        {
            var items = Game.World.ItemEntities.OrderBy(entity => entity.CellLocation.DistanceSquaredTo(CellLocation));
            foreach (var item in items)
            {
                if (item.TargetedByEntity == null)
                {
                    AssignPickupItemPlan(item);
                    return true;
                }
            }
            return false;
        }

        private bool FindContainerToDepositItemsAndAssignPlan()
        {
            //var container = Game.World.ContainerEntities.OrderBy(entity => entity.CellLocation.DistanceSquaredTo(CellLocation)).FirstOrDefault();
            var container = Game.World.ContainerEntities.Where(entity => Inventory.CanTransferAny(entity.Inventory)).OrderBy(entity => entity.CellLocation.DistanceSquaredTo(CellLocation)).FirstOrDefault();
            if (container != null)
            {
                if(Inventory.CanTransferAny(container.Inventory))
                {
                    AssignDepositItemsPlan(container);
                    return true;
                }
            }
            return false;
        }

        private bool FindTreeToAttackAndAssignPlan()
        {
            var entities = Game.World.EntitiesMarkedForDestruction.Enumerate().SelectMany(pair => pair.Value);
            var entityToAttack = entities.OrderBy(entity => entity.CellLocation.DistanceSquaredTo(CellLocation)).FirstOrDefault(entity => !(entity as RefinableEntity)?.IsTargetOfEntities.Any() ?? true);
            if (entityToAttack != null)
            {
                AssignAttackEntityPlan(entityToAttack);
                return true;
            }
            return false;
        }

        public override void Update(TimeSpan elapsedTime)
        {
            base.Update(elapsedTime);

            if (PlannedAction == null && !PlannedPath.Any())
            {
                planAssignmentTimer.Update(elapsedTime);
            }

            if(IsAttacking)
            {
                attackDurationTimer.Update(elapsedTime);
            }
        }
    }
}
