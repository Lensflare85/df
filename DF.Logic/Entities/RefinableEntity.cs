﻿using DF.Logic.Entities.Types;
using System;
using System.Collections.Generic;

namespace DF.Logic.Entities
{
    public sealed class RefinableEntity : WorldEntity, IDisposable
    {
        public readonly RefinableType RefinableType;

        public readonly List<MobileEntity> IsTargetOfEntities = new List<MobileEntity>(); 

        public RefinableEntity(RefinableType refinableType) : base()
        {
            Passable = false;
            RefinableType = refinableType;
        }

        public void Dispose()
        {
            foreach(var targetingEntity in IsTargetOfEntities.ToArray())
            {
                targetingEntity.CancelPlan();
            }

            IsTargetOfEntities.Clear();
        }
    }
}
