﻿using DF.Logic.Entities.Components;
using DF.Logic.Entities.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Entities
{
    public sealed class BuildingEntity : WorldEntity, IDisposable
    {
        public readonly BuildingType BuildingType;

        public ItemGrabber ItemGrabber;

        public BuildingEntity(BuildingType buildingType) : base()
        {
            Passable = false;
            BuildingType = buildingType;
        }

        public void Dispose()
        {
            if(ItemGrabber != null)
            {
                Game.World.ItemGrabberMarkedLocations.RemoveValue(ItemGrabber.InputMarker);
                Game.World.ItemGrabberMarkedLocations.RemoveValue(ItemGrabber.OutputMarker);

                ItemGrabber.WorldEntity = null;
                ItemGrabber.InputMarker = null;
                ItemGrabber.OutputMarker = null;
                ItemGrabber = null;
            }

            if(HitPoints != null)
            {
                HitPoints.WorldEntity = null;
                HitPoints = null;
            }

            if(Inventory != null)
            {
                Inventory.WorldEntity = null;
                Inventory = null;
            }

            Game = null;
        }

        public override void Update(TimeSpan elapsedTime)
        {
            base.Update(elapsedTime);

            ItemGrabber?.Update(elapsedTime);
        }
    }
}
