﻿namespace DF.Logic.Entities.EntityAction
{
    public sealed class AttackEntityAction : ActionBase
    {
        public WorldEntity EntityToAttack;
    }
}
