﻿using DF.Logic.Entities.Components;
using System;
using System.Numerics;
using Util;

namespace DF.Logic.Entities
{
    public class WorldEntity : Entity
    {
        public int Variant;

        public IntVector2 CellLocation;

        public const int OffsetRange = 10000;
        public IntVector2 Offset;

        private Vector2 OffsetRange2 = new Vector2(OffsetRange, OffsetRange);

        public Vector2 RealOffset => (Offset - OffsetRange2 * 0.5f) / OffsetRange2;

        public HitPoints HitPoints;

        public IItemsInventory Inventory;

        public bool Passable;

        public void DealDamageTo(WorldEntity entity, int damagePoints)
        {
            entity.HitPoints?.TakeDamageFrom(this, damagePoints);
        }

        public override void Update(TimeSpan elapsedTime)
        {
            
        }
    }
}
