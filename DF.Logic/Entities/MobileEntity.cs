﻿using DF.Logic.Entities.Components;
using DF.Logic.Entities.EntityAction;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Util;
using Util.Random;

namespace DF.Logic.Entities
{
    public class MobileEntity : WorldEntity
    {
        protected RandomNumberGenerator random;

        public IntVector2 NextOffsetTarget = IntVector2.Zero;

        public int MaxVelocity { get; protected set; } = 40;

        public LinkedList<IntVector2> PlannedPath = new LinkedList<IntVector2>();

        public ActionBase PlannedAction;

        protected ActionTimer attackIntervalTimer;
        protected ActionTimer attackDurationTimer;

        public bool IsAttacking { get; private set; }

        public MobileEntity(int seed) : base()
        {
            random = new RandomNumberGenerator(seed);
            Offset = RandomOffset();
            CalculateNextOffsetTarget();

            attackDurationTimer = new ActionTimer(this, intervalInSeconds: 0.18f, action: HandleAttackEnded);
        }

        protected IntVector2 RandomOffset()
        {
            return new IntVector2(random.Next(OffsetRange), random.Next(OffsetRange)) - new IntVector2(OffsetRange / 2, OffsetRange / 2);
        }

        protected void CalculateNextOffsetTarget()
        {
            if (PlannedAction is PickupItemAction pickupItemAction)
            {
                if (pickupItemAction.Item.CellLocation == this.CellLocation)
                {
                    NextOffsetTarget = pickupItemAction.Item.Offset;
                    return;
                }
            }

            NextOffsetTarget = RandomOffset();
            //NextOffsetTarget = IntVector2.Zero;
        }

        void MoveIntoDirection(IntVector2 direction, TimeSpan elapsedTime)
        {
            //Offset += direction * (int)(elapsedTime.TotalSeconds * OffsetRange * MaxVelocity * 0.1);
            var v = (direction * OffsetRange * 2 - Offset + NextOffsetTarget).Vector2;
            v = v / v.Length() * OffsetRange * (float)elapsedTime.TotalSeconds * MaxVelocity * 0.1f;
            Offset += new IntVector2(v);
        }

        private void PerformActions()
        {
            const int touchDist = OffsetRange / 10;

            switch(PlannedAction)
            {
                case PickupItemAction itemPickupAction:
                    {
                        var item = itemPickupAction.Item;
                        if (item.CellLocation == this.CellLocation && item.Offset.DistanceTo(Offset) < touchDist)
                        {
                            var added = Inventory?.Add(item.ItemType, 1) ?? 0;
                            if (added > 0)
                            {
                                Game.World.EntitiesToDestroy.Add(item);
                            }
                        }
                    }
                    break;
                case DepositItemsAction depositItemsAction:
                    {
                        var container = depositItemsAction.TargetEntity;
                        var cellOffset = container.CellLocation - this.CellLocation;
                        if (cellOffset == IntVector2.Zero || Game.World.PathFinder.PossibleOffsets.Contains(cellOffset))
                        {
                            /*for (int i = 0; i < Inventory.NumberOfSlots; ++i)
                            {
                                while (Inventory[i] != null)
                                {
                                    var itemType = Inventory[i].ItemType;
                                    Inventory.RemoveOneItemFromSlot(i); //TODO: before removing, check if the container will be able to hold it.
                                    var added = depositItemsAction.TargetEntity.Inventory.AddItems(itemType, 1);
                                    if (added < 1)
                                    {
                                        break;
                                    }
                                }
                            }*/
                            Inventory.TransferTo(depositItemsAction.TargetEntity.Inventory);
                            CancelPlan();
                        }
                    }
                    break;
            }
        }

        public void CancelPlan()
        {
            switch (PlannedAction)
            {
                case PickupItemAction pickupItemAction:
                    pickupItemAction.Item.TargetedByEntity = null;
                    break;
                case AttackEntityAction attackEntityAction:
                    if (attackEntityAction.EntityToAttack is RefinableEntity refinableEntity)
                    {
                        refinableEntity.IsTargetOfEntities.Remove(this);
                    }
                    break;
            }
            PlannedAction = null;
            PlannedPath = new LinkedList<IntVector2>();
        }

        public void AssignDepositItemsPlan(WorldEntity targetEntity)
        {
            CancelPlan();

            var offset = targetEntity.CellLocation - this.CellLocation;

            void assignPlannedAction()
            {
                PlannedAction = new DepositItemsAction() { TargetEntity = targetEntity };
            }

            if (Game.World.PathFinder.PossibleOffsets.Contains(offset))
            {
                assignPlannedAction();
            }
            else
            {
                PlannedPath = new LinkedList<IntVector2>(Game.World.PathFinder.FindPath(CellLocation, targetEntity.CellLocation, false, location => Game.World.CellIsPassableForEntity(location, this)));
                if (PlannedPath.Any())
                {
                    assignPlannedAction();
                }
                else
                {
                    Debug.WriteLine($"Trying to assign {nameof(DepositItemsAction)}: Path not found or too long.");
                }
            }
        }

        public void AssignPickupItemPlan(ItemEntity item)
        {
            CancelPlan();

            if(item.CellLocation == this.CellLocation)
            {
                item.TargetedByEntity = this;
                PlannedAction = new PickupItemAction() { Item = item };
                CalculateNextOffsetTarget();
            }
            else
            {
                PlannedPath = new LinkedList<IntVector2>(Game.World.PathFinder.FindPath(CellLocation, item.CellLocation, true, location => Game.World.CellIsPassableForEntity(location, this)));
                if(PlannedPath.Any())
                {
                    item.TargetedByEntity = this;
                    PlannedAction = new PickupItemAction() { Item = item };
                }
                else
                {
                    Debug.WriteLine($"Trying to assign {nameof(PickupItemAction)}: Path not found or too long.");
                }
            }
        }

        public void AssignAttackEntityPlan(WorldEntity entity)
        {
            CancelPlan();

            var offset = entity.CellLocation - this.CellLocation;

            void assignPlannedAction()
            {
                PlannedAction = new AttackEntityAction() { EntityToAttack = entity };
                if(entity is RefinableEntity refinableEntity)
                {
                    refinableEntity.IsTargetOfEntities.Add(this);
                }
                attackIntervalTimer?.Dispose();
                attackIntervalTimer = new ActionTimer(this, 0.3f, delegate {
                    Attack(entity);
                    var stillTheSamePlan = PlannedAction is AttackEntityAction attackEntityAction && entity == attackEntityAction.EntityToAttack && Game.World.Entities.ContainsValueForKey(entity.CellLocation, entity);
                    if(!stillTheSamePlan)
                    {
                        PlannedAction = null;
                        attackIntervalTimer?.Dispose();
                        attackIntervalTimer = null;
                    }
                });
            }

            if (Game.World.PathFinder.PossibleOffsets.Contains(offset))
            {
                assignPlannedAction();
            }
            else
            {
                PlannedPath = new LinkedList<IntVector2>(Game.World.PathFinder.FindPath(CellLocation, entity.CellLocation, false, location => Game.World.CellIsPassableForEntity(location, this)));
                if (PlannedPath.Any())
                {
                    assignPlannedAction();
                }
                else
                {
                    Debug.WriteLine($"Trying to assign {nameof(AttackEntityAction)}: Path not found or too long.");
                }
            }
        }

        private void Attack(WorldEntity entity)
        {
            IsAttacking = true;
            attackDurationTimer.ResetTimer();

            DealDamageTo(entity, 5); //TODO: decide how many damage points to deal
        }

        private void HandleAttackEnded()
        {
            IsAttacking = false;
        }

        public virtual void HandleNewCellLocation(IntVector2 newLocation)
        {
            if(PlannedPath.Any() && PlannedPath.First() == newLocation)
            {
                PlannedPath.RemoveFirst();
            }
            CellLocation = newLocation;
        }

        public override void Update(TimeSpan elapsedTime)
        {
            base.Update(elapsedTime);
            if (PlannedPath.Any())
            {
                var nextLocation = PlannedPath.First();
                if (World.CellIsNeighbor(nextLocation, CellLocation))
                {
                    var direction = (nextLocation - CellLocation).Direction9State;
                    MoveIntoDirection(direction, elapsedTime);
                }
            }
            else
            {
                var distToTarget = (NextOffsetTarget - Offset).Length();
                var travelDistNextStep = OffsetRange * (float)elapsedTime.TotalSeconds * MaxVelocity * 0.1f;
                if (distToTarget > travelDistNextStep)
                {
                    MoveIntoDirection(IntVector2.Zero, elapsedTime);
                }

                if(PlannedAction is AttackEntityAction attackEntityAction)
                {
                    attackIntervalTimer?.Update(elapsedTime);
                }
            }

            PerformActions();
        }
    }
}
