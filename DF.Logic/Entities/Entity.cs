﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Entities
{
    public abstract class Entity
    {
        public Game Game;

        public abstract void Update(TimeSpan elapsedTime);
    }
}
