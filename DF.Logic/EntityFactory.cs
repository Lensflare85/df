﻿using DF.Logic.Entities;
using DF.Logic.Entities.Components;
using DF.Logic.Entities.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace DF.Logic
{
    public sealed class EntityFactory
    {
        private Game game;

        public EntityFactory(Game game)
        {
            this.game = game;
        }

        public BuildingEntity CreateBuilding(BuildingType type, IntVector2 location)
        {
            var building = new BuildingEntity(type)
            {
                Game = game,
                CellLocation = location,
                Offset = new IntVector2(WorldEntity.OffsetRange/2, WorldEntity.OffsetRange / 2),
            };
            building.HitPoints = new HitPoints(building)
            {
                MaxHitPoints = type.MaxHitpoints,
                CurrentHitPoints = type.MaxHitpoints,
            };

            if(type.NumberOfInventorySlots > 0)
            {
                if(type.InventorySlotsAreStacked)
                {
                    building.Inventory = new StackedItemsInventory(building, type.NumberOfInventorySlots);
                }
                else
                {
                    building.Inventory = new SingleItemsInventory(building, type.NumberOfInventorySlots);
                }
            }

            if(type.IsItemGrabber)
            {
                var itemGrabberComponent = new ItemGrabber()
                {
                    WorldEntity = building
                };
                itemGrabberComponent.InputMarker = new ItemGrabberInputMarker(itemGrabberComponent, location - IntVector2.UnitX);
                itemGrabberComponent.OutputMarker = new ItemGrabberOutputMarker(itemGrabberComponent, location + IntVector2.UnitX - IntVector2.UnitY);
                building.ItemGrabber = itemGrabberComponent;
            }

            return building;
        }
    }
}
