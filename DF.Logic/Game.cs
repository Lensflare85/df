﻿using DF.Logic.Entities.Components;
using DF.Logic.Entities.Types;
using DF.Logic.Interfaces;
using DF.Logic.Resource;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace DF.Logic
{
    public sealed class Game
    {
        public ItemTypeDefinition ItemTypes { get; private set; }
        public BuildingTypeDefinition BuildingTypes { get; private set; }
        public RefinableTypeDefinition RefinableTypes { get; private set; }

        public World World;
        public EntityFactory EntityFactory { get; private set; }
        public IResourceCreator ResourceCreator;
        public Resources Resources;
        public IFileSystem FileSystem;
        public Player Player { get; private set; }

        public Game()
        {
            Player = new Player() { Name = "Dummy" };

            World = new World();
            World.Game = this;

            ItemTypes = new ItemTypeDefinition();
            BuildingTypes = new BuildingTypeDefinition();
            RefinableTypes = new RefinableTypeDefinition();

            World.RegisteredPlayers.Add(Player);

            EntityFactory = new EntityFactory(this);

            TestInventory();
        }

        private void TestInventory()
        {
            Debug.WriteLine("###");

            var inventoryA = new StackedItemsInventory(null, 5);
            inventoryA[0] = new ItemStack(ItemTypes.Dummy, ItemTypes.Dummy.MaxInventoryStackSize);
            inventoryA[2] = new ItemStack(ItemTypes.Dummy, ItemTypes.Dummy.MaxInventoryStackSize / 2);
            inventoryA[3] = new ItemStack(ItemTypes.Wood, ItemTypes.Wood.MaxInventoryStackSize);
            Debug.WriteLine("A " + inventoryA);

            var inventoryB = new StackedItemsInventory(null, 3);
            inventoryB[0] = new ItemStack(ItemTypes.Dummy, ItemTypes.Dummy.MaxInventoryStackSize / 2);
            inventoryB[2] = new ItemStack(ItemTypes.Wood, ItemTypes.Wood.MaxInventoryStackSize / 2);
            Debug.WriteLine("B " + inventoryB);

            var transferred = inventoryA.TransferTo(inventoryB);

            Debug.WriteLine("transferred " + transferred);
            Debug.WriteLine("");

            Debug.WriteLine("A " + inventoryA);

            Debug.WriteLine("B " + inventoryB);

            Debug.WriteLine("---");
        }

        bool loaded = false;

        public void LoadWorld()
        {
            /*foreach (var chunkLocation in Grid.CoordinatesInSpiral(IntVector2.Zero, 20)) //TODO: spiral radius 20 => ruckeln wegen sortierung nach distanz (zu viele entities)
            {
                World.Generator.GenerateCellChunk(chunkLocation);
            }*/
            World.Generator.GenerateCellChunk(IntVector2.Zero);
            loaded = true;
        }

        public void Update(TimeSpan elapsedTime)
        {
            if (!loaded)
            {
                LoadWorld();
            }
            World.Update(elapsedTime);
        }
    }
}
