﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic.Screen
{
    public sealed class SmoothViewPort : ViewPort
    {
        float spaceZoomSpeed;

        public void AccelerateSpaceZoom(float zoomChange)
        {
            spaceZoomSpeed += zoomChange;
        }

        public void Update(TimeSpan elapsedTime)
        {
            float maxSpaceZoom = 4;
            float minSpaceZoom = 0.25f * 0.5f;

            intermediateSpaceZoom *= (float)Math.Pow(10, spaceZoomSpeed * 0.015f * elapsedTime.TotalSeconds);
            spaceZoomSpeed *= (float)Math.Pow(10, -9.0f * elapsedTime.TotalSeconds);

            if(intermediateSpaceZoom < minSpaceZoom)
            {
                intermediateSpaceZoom = minSpaceZoom;
            }
            else if (intermediateSpaceZoom > maxSpaceZoom)
            {
                intermediateSpaceZoom = maxSpaceZoom;
            }
        }
    }
}
