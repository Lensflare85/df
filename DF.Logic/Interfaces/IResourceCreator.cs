﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;
using Windows.UI;

namespace DF.Logic.Interfaces
{
    public interface IResourceCreator
    {
        object ResourceCreator { get; }

        IBitmap CreateBitmapFromColors(Color[] colors, int width, int height);
        IBitmap CreateBitmapFromColors(Color[] colors, IntVector2 size);

        Task<IBitmap> CreateBitmapFromFilePathAsync(string filePath);
    }
}
