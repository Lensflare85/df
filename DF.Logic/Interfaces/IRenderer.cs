﻿using DF.Logic.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Util;
using Windows.UI;

namespace DF.Logic.Interfaces
{
    public interface IRenderer
    {
        void BeginClippingRect(IntVector2 location, IntVector2 size);
        void EndClippingRect();

        void BeginSpriteBatch();
        void EndSpriteBatch();

        void FillRectangle(Vector2 location, Vector2 size, Color color);
        void DrawRectangle(Vector2 location, Vector2 size, Color color, float thickness = 1);

        void DrawLine(Vector2 start, Vector2 end, Color color, float thickness = 1);

        void DrawBitmap(IBitmap bitmap, Vector2 location, Vector2 size);
        void DrawBitmap(IBitmap bitmap, Vector2 location, Vector2 size, float rotationAboutCenter);

        void DrawBitmapFromSpriteBatch(IBitmap bitmap, Vector2 location, Vector2 size);
        void DrawBitmapFromSpriteBatch(IBitmap bitmap, Vector2 location, Vector2 size, float rotationAngle, Vector2 rotationPoint);

        void DrawBitmapFromSpriteSheetBatch(SpriteSheet spriteSheet, int index, Vector2 location, Vector2 size, Vector4? tint = null);

    }
}
