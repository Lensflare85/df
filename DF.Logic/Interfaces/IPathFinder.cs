﻿using System;
using System.Collections.Generic;
using Util;

namespace DF.Logic.Interfaces
{
    public interface IPathFinder
    {
        IntVector2[] PossibleOffsets { get; }
        IntVector2[] PossibleDiagonalOffsets { get; }

        IEnumerable<IntVector2> FindPath(IntVector2 start, IntVector2 end, bool includingEnd, Func<IntVector2, bool> isPassable);
    }
}
