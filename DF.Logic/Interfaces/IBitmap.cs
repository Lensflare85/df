﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace DF.Logic.Interfaces
{
    public interface IBitmap : IDisposable
    {
        object Bitmap { get; }

        IntVector2 SizeInPixels { get; }
    }
}
