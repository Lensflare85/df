﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.Logic
{
    public struct WorldCell
    {
        public Ground Ground;
        public Wall Wall;

        public int GroundVariant;
    }

    public enum Ground
    {
        None = 0,
        Earth = 1,
        Grass = 2,
        Sand = 3,
    }

    public enum Wall
    {
        None = 0,
        Earth = 1,
    }
}
