﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace DF
{
    public class FileSystem : IFileSystem
    {
        public string ExecutionDirectory()
        {
            return Directory.GetCurrentDirectory();
        }
    }
}
