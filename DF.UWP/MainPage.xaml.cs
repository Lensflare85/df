﻿using DF.Logic;
using DF.Logic.Interfaces;
using DF.Logic.Resource;
using DF.Logic.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" wird unter https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x407 dokumentiert.

namespace DF
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Game game;
        GameView gameView = new GameView();
        Renderer renderer;

        double canvasWidth = 0;
        double canvasHeight = 0;

        int frameCounter = 0;
        int fps = 0;
        double secondsCounter = 0;

        ResourceCreator resourceCreator;

        public MainPage()
        {
            this.InitializeComponent();

            fpsTextBlock.Text = "";
        }

        bool hasCreatedResources = false;

        private async void canvas_CreateResources(Microsoft.Graphics.Canvas.UI.Xaml.CanvasAnimatedControl sender, Microsoft.Graphics.Canvas.UI.CanvasCreateResourcesEventArgs args)
        {
            if (hasCreatedResources)
            {
                Debug.WriteLine("second call to canvas_CreateResources()");
            }

            Debug.WriteLine("CreateResources ");

            resourceCreator = new ResourceCreator();
            resourceCreator.CanvasResourceCreator = sender;
            
            game = new Game();
            game.ResourceCreator = resourceCreator;
            game.Resources = new Resources();
            game.FileSystem = new FileSystem();

            renderer = new Renderer();

            gameView.Game = game;
            gameView.Renderer = renderer;

            await game.Resources.LoadAsync(resourceCreator, game.FileSystem);

            //game.LoadWorld();

            hasCreatedResources = true;
        }

        private void canvas_Draw(Microsoft.Graphics.Canvas.UI.Xaml.ICanvasAnimatedControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasAnimatedDrawEventArgs args)
        {
            ++frameCounter;

            using (var drawingSession = args.DrawingSession)
            {
                renderer.DrawingSession = drawingSession;
                gameView.PrepareToDraw(new Vector2((float)canvasWidth, (float)canvasHeight));
                gameView.Draw();
                //Debug.WriteLine("slow " + args.Timing.IsRunningSlowly);
            }
        }

        private async void canvas_Update(Microsoft.Graphics.Canvas.UI.Xaml.ICanvasAnimatedControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasAnimatedUpdateEventArgs args)
        {
            var elapsedTime = args.Timing.ElapsedTime;

            if (hasCreatedResources)
            {
                gameView.Update(elapsedTime);
                game.Update(elapsedTime);
            }

            var totalSeconds = args.Timing.ElapsedTime.TotalSeconds;
            secondsCounter += totalSeconds;
            if (secondsCounter >= 1)
            {
                secondsCounter -= 1;
                fps = frameCounter;
                frameCounter = 0;
            }

            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, delegate {
                fpsTextBlock.Text = "FPS: " + fps;
            });
        }

        private void canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            canvasWidth = e.NewSize.Width;
            canvasHeight = e.NewSize.Height;
        }

        private void canvas_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            var p = e.GetCurrentPoint(canvas).Position.ToVector2();
            //Debug.WriteLine("PointerMoved " + p.X + " " + p.Y);
            gameView.ProcessNewPointerPosition(p);

            //App.CurrentApp.Window.CoreWindow.PointerCursor = null;
            //App.CurrentApp.Window.CoreWindow.PointerPosition = new Point(400, 400);
        }

        private void canvas_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var p = e.GetCurrentPoint(canvas).Position.ToVector2();

            var properties = e.GetCurrentPoint(canvas).Properties;
            if (e.Pointer.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Mouse)
            {
                if(properties.IsLeftButtonPressed)
                {
                    gameView.HandleLeftClick(p);
                }

                gameView.DirectScrollHolding = properties.IsMiddleButtonPressed;
                gameView.IndirectScrollHolding = properties.IsRightButtonPressed;
                if(gameView.IndirectScrollHolding)
                {
                    gameView.IndirectScrollStartPosition = p;
                }
            }
            if (e.Pointer.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Touch)
            {
                gameView.DirectScrollHolding = properties.TouchConfidence;
            }

            //Debug.WriteLine("PointerPressed " + p.X + " " + p.Y);
        }

        private void canvas_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            var properties = e.GetCurrentPoint(canvas).Properties;
            if (e.Pointer.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Mouse)
            {
                gameView.DirectScrollHolding = properties.IsMiddleButtonPressed;
                gameView.IndirectScrollHolding = properties.IsRightButtonPressed;
            }
            if (e.Pointer.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Touch)
            {
                gameView.DirectScrollHolding = properties.TouchConfidence;
            }

            //var p = e.GetCurrentPoint(canvas).Position.ToVector2();
            //Debug.WriteLine("PointerReleased " + p.X + " " + p.Y);
        }

        private void canvas_Holding(object sender, HoldingRoutedEventArgs e)
        {
            var p = e.GetPosition(canvas).ToVector2();
            Debug.WriteLine("Holding " + p.X + " " + p.Y);
        }

        private void canvas_DragOver(object sender, DragEventArgs e)
        {
            var p = e.GetPosition(canvas).ToVector2();
            Debug.WriteLine("DragOver " + p.X + " " + p.Y);
        }

        private void canvas_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            var p = e.Position.ToVector2();
            Debug.WriteLine("ManipulationDelta " + p.X + " " + p.Y);
        }

        private void canvas_PointerWheelChanged(object sender, PointerRoutedEventArgs e)
        {
            var delta = e.GetCurrentPoint(canvas).Properties.MouseWheelDelta;
            gameView.ProcessWheelDelta(delta);
            Debug.WriteLine("PointerWheelChanged " + delta);
        }

        private void canvas_Loaded(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Loaded ");
        }
    }
}
