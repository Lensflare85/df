﻿using DF.Logic;
using DF.Logic.Interfaces;
using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;
using Windows.UI;

namespace DF
{
    public class ResourceCreator : IResourceCreator
    {
        public ICanvasResourceCreator CanvasResourceCreator;

        object IResourceCreator.ResourceCreator => CanvasResourceCreator;

        public IBitmap CreateBitmapFromColors(Color[] colors, int width, int height)
        {
            return new Bitmap { CanvasBitmap = CanvasBitmap.CreateFromColors(CanvasResourceCreator, colors, width, height) };
        }

        public IBitmap CreateBitmapFromColors(Color[] colors, IntVector2 size) => CreateBitmapFromColors(colors, size.X, size.Y);

        public async Task<IBitmap> CreateBitmapFromFilePathAsync(string filePath)
        {
            var bitmap = await CanvasBitmap.LoadAsync(CanvasResourceCreator, filePath);
            /*
            var size = bitmap.SizeInPixels;
            var colors = bitmap.GetPixelColors();
            bitmap.Dispose();
            colors[10] = Colors.Red;
            bitmap = CanvasBitmap.CreateFromColors(CanvasResourceCreator, colors, (int)size.Width, (int)size.Height);
            */
            return new Bitmap { CanvasBitmap = bitmap };
        }
    }
}
