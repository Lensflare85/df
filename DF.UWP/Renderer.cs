﻿using DF.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using Windows.UI;
using Microsoft.Graphics.Canvas;
using DF.Logic.Interfaces;
using Windows.Foundation;
using DF.Logic.Resource;
using Util;

namespace DF
{
    public sealed class Renderer : IRenderer
    {
        public CanvasDrawingSession DrawingSession;

        private CanvasActiveLayer layer;
        private CanvasSpriteBatch spriteBatch;

        public void BeginClippingRect(IntVector2 location, IntVector2 size)
        {
            layer = DrawingSession.CreateLayer(1, location.ToRectWithSize(size));
        }

        public void EndClippingRect()
        {
            layer.Dispose();
            layer = null;
        }

        public void BeginSpriteBatch()
        {
            spriteBatch = DrawingSession.CreateSpriteBatch(CanvasSpriteSortMode.None, CanvasImageInterpolation.NearestNeighbor);
        }

        public void EndSpriteBatch()
        {
            spriteBatch.Dispose();
            spriteBatch = null;
        }

        public void DrawLine(Vector2 start, Vector2 end, Color color, float thickness = 1)
        {
            DrawingSession.DrawLine(start, end, color, thickness);
        }

        public void DrawRectangle(Vector2 location, Vector2 size, Color color, float thickness = 1)
        {
            DrawingSession.DrawRectangle(location.X, location.Y, size.X, size.Y, color, thickness);
        }

        public void FillRectangle(Vector2 location, Vector2 size, Color color)
        {
            DrawingSession.FillRectangle(location.X, location.Y, size.X, size.Y, color);
        }

        public void DrawBitmap(IBitmap bitmap, Vector2 location, Vector2 size)
        {
            var canvasBitmap = (CanvasBitmap)bitmap.Bitmap;
            var bitmapSize = canvasBitmap.SizeInPixels;
            var rotationCenter = new Vector3(size.X * 0.5f, size.Y * 0.5f, 0);
            DrawingSession.DrawImage(
                canvasBitmap,
                location.ToRectWithSize(size),
                new Rect(0, 0, bitmapSize.Width, bitmapSize.Height),
                1);
        }

        public void DrawBitmap(IBitmap bitmap, Vector2 location, Vector2 size, float rotationAboutCenter)
        {
            var canvasBitmap = (CanvasBitmap)bitmap.Bitmap;
            var bitmapSize = canvasBitmap.SizeInPixels;
            var rotationCenter = new Vector3(size.X * 0.5f + location.X, size.Y * 0.5f + location.Y, 0);
            DrawingSession.DrawImage(
                canvasBitmap,
                location.ToRectWithSize(size),
                new Rect(0, 0, bitmapSize.Width, bitmapSize.Height),
                1,
                CanvasImageInterpolation.NearestNeighbor, Matrix4x4.CreateRotationZ(rotationAboutCenter, rotationCenter));
        }

        public void DrawBitmapFromSpriteBatch(IBitmap bitmap, Vector2 location, Vector2 size)
        {
            var canvasBitmap = (CanvasBitmap)bitmap.Bitmap;
            //var bitmapSize = canvasBitmap.SizeInPixels;
            spriteBatch.Draw(canvasBitmap, location.ToRectWithSize(size));
        }

        public void DrawBitmapFromSpriteBatch(IBitmap bitmap, Vector2 location, Vector2 size, float rotationAngle, Vector2 rotationPoint)
        {
            var canvasBitmap = (CanvasBitmap)bitmap.Bitmap;
            spriteBatch.Draw(canvasBitmap, location, Vector4.One, rotationPoint, rotationAngle, size / bitmap.SizeInPixels, CanvasSpriteFlip.None);
        }

        public void DrawBitmapFromSpriteSheetBatch(SpriteSheet spriteSheet, int index, Vector2 location, Vector2 size, Vector4? tint = null)
        {
            var canvasBitmap = (CanvasBitmap)spriteSheet.Bitmap.Bitmap;
            var region = spriteSheet.Regions[index];
            var bitmapSize = canvasBitmap.SizeInPixels;
            var regionSize = region.Size();
            var scale = size / regionSize;
            var offset = location - (region.ReferencePoint - region.Start) * scale;
            if (tint == null)
            {
                spriteBatch.DrawFromSpriteSheet(canvasBitmap, offset.ToRectWithSize(size), region.Start.ToRectWithSize(regionSize));
            }
            else
            {
                spriteBatch.DrawFromSpriteSheet(canvasBitmap, offset.ToRectWithSize(size), region.Start.ToRectWithSize(regionSize), tint.Value);
            }
        }
    }
}
