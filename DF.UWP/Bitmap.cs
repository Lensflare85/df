﻿using DF.Logic.Interfaces;
using Microsoft.Graphics.Canvas;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace DF
{
    public class Bitmap : IBitmap
    {
        public CanvasBitmap CanvasBitmap;

        public IntVector2 SizeInPixels
        {
            get
            {
                var size = CanvasBitmap.SizeInPixels;
                return new IntVector2((int)size.Width, (int)size.Height);
            }
        }

        object IBitmap.Bitmap => CanvasBitmap;

        public void Dispose()
        {
            CanvasBitmap.Dispose();
            CanvasBitmap = null;
        }
    }
}
