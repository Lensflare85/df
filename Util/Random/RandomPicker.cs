﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util.Random
{
    public sealed class RandomPicker
    {
        IRandomNumberGenerator randomNumberGenerator;

        public RandomPicker(IRandomNumberGenerator randomNumberGenerator)
        {
            this.randomNumberGenerator = randomNumberGenerator;
        }

        public T PickFrom<T>(params T[] possibleDirections)
        {
            return possibleDirections[randomNumberGenerator.Next(possibleDirections.Length)];
        }
    }
}
