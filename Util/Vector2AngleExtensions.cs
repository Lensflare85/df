﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Util
{
    public static class Vector2AngleExtensions
    {
        public static float AngleDeltaTo(this Vector2 vector1, Vector2 vector2)
        {
            return (float)Math.Atan2(vector1.X * vector2.Y - vector1.Y * vector2.X, vector1.X * vector2.X + vector1.Y * vector2.Y); //atan2(crossProduct(vector1, vector2), dotProduct(vector1, vector2))
        }

        public static Vector2 RotatedBy(this Vector2 vector, float angleDelta)
        {
            var x = (float)Math.Cos(angleDelta);
            var y = (float)Math.Sin(angleDelta);
            return new Vector2(x * vector.X - y * vector.Y, y * vector.X + x * vector.Y);
        }
    }
}
