﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public sealed class ListDictionary<TKey, TValue>
    {
        private Dictionary<TKey, List<TValue>> dictionary = new Dictionary<TKey, List<TValue>>();

        public IEnumerable<TValue> this[TKey key]
        {
            get => dictionary.GetOrDefault(key);
            set
            {
                if(value == null || !value.Any())
                {
                    dictionary.Remove(key);
                }
                else
                {
                    dictionary[key] = value.ToList();
                }
            }
        }

        public void AddValue(TKey key, TValue value)
        {
            var values = dictionary.GetOrDefault(key);
            if(values == null)
            {
                dictionary[key] = new List<TValue>() { value };
            }
            else
            {
                values.Add(value);
            }
        }

        public void AddValues(TKey key, IEnumerable<TValue> range)
        {
            var values = dictionary.GetOrDefault(key);
            if (values == null)
            {
                dictionary[key] = range.ToList();
            }
            else
            {
                values.AddRange(range);
            }
        }

        public bool Remove(TKey key)
        {
            return dictionary.Remove(key);
        }

        public bool RemoveValueForKey(TKey key, TValue value)
        {
            var removed = false;
            var values = dictionary.GetOrDefault(key);
            if(values != null)
            {
                removed = values.Remove(value);

                if(!values.Any())
                {
                    dictionary.Remove(key);
                }
            }

            return removed;
        }

        public IEnumerable<TValue> RemoveValuesForKey(TKey key, IEnumerable<TValue> range)
        {
            List<TValue> removed = new List<TValue>();

            var values = dictionary.GetOrDefault(key);
            if (values != null)
            {
                foreach(var value in range)
                {
                    var wasInList = values.Remove(value);
                    if(wasInList)
                    {
                        removed.Add(value);
                    }
                }

                if (!values.Any())
                {
                    dictionary.Remove(key);
                }
            }

            return removed;
        }

        public IEnumerable<TKey> RemoveValue(TValue value)
        {
            var removed = new List<TKey>();

            var keysToRemove = new List<TKey>();

            foreach (var pair in dictionary)
            {
                var wasInList = pair.Value.Remove(value);
                if (wasInList)
                {
                    removed.Add(pair.Key);
                }

                if (!pair.Value.Any())
                {
                    keysToRemove.Add(pair.Key);
                }
            }

            foreach (var keyToRemove in keysToRemove)
            {
                dictionary.Remove(keyToRemove);
            }

            return removed;
        }

        public ListDictionary<TKey, TValue> RemoveValues(IEnumerable<TValue> range)
        {
            var removed = new ListDictionary<TKey, TValue>();

            var keysToRemove = new List<TKey>();

            foreach(var pair in dictionary)
            {
                foreach(var value in range)
                {
                    var wasInList = pair.Value.Remove(value);
                    if(wasInList)
                    {
                        removed.AddValue(pair.Key, value);
                    }
                }

                if(!pair.Value.Any())
                {
                    keysToRemove.Add(pair.Key);
                }
            }

            foreach(var keyToRemove in keysToRemove)
            {
                dictionary.Remove(keyToRemove);
            }

            return removed;
        }

        public IEnumerable<KeyValuePair<TKey, List<TValue>>> Enumerate()
        {
            return dictionary;
        }

        /*public IEnumerable<(key: TKey, values: IEnumerable<TValue>)> Enumerate()
        {
            return dictionary.Select(pair => (key: pair.Key, values: pair.Value.AsEnumerable()));
        }*/

        public bool IsEmpty()
        {
            return !dictionary.Any();
        }

        public IEnumerable<TKey> KeysContainingValue(TValue value)
        {
            return dictionary.Where(pair => pair.Value.Contains(value)).Select(pair => pair.Key);
        }

        public bool ContainsValueForKey(TKey key, TValue value)
        {
            return dictionary.GetOrDefault(key)?.Contains(value) ?? false;
        }
    }
}
