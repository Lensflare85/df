﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Util
{
    public static class RectExtension
    {
        public static Vector2 Location(this Rect rect)
        {
            return new Vector2((float)rect.X, (float)rect.Y);
        }

        public static Vector2 Size(this Rect rect)
        {
            return new Vector2((float)rect.Width, (float)rect.Height);
        }
    }
}
