﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util.Map
{
    public interface IMap2<T>
    {
        int Width { get; }
        int Height { get; }

        T this[int x, int y] { get; set; }

        T this[IntVector2 coordinates] { get; set; }

        IMap2<T> Clone();

        IEnumerable<T> AllValues();
    }
}
