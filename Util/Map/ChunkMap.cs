﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util.Map
{
    public sealed class ChunkMap<T> where T : class
    {
        public IntVector2 ChunkSize { get; private set; }

        Dictionary<IntVector2, T> chunks = new Dictionary<IntVector2, T>();

        public ChunkMap(IntVector2 chunkSize)
        {
            ChunkSize = chunkSize;
        }

        public T this[IntVector2 chunkCoordinates]
        {
            get
            {
                return chunks.GetOrDefault(chunkCoordinates);
            }
            set
            {
                if (value == null)
                {
                    chunks.Remove(chunkCoordinates);
                }
                else
                {
                    chunks[chunkCoordinates] = value;
                }
            }
        }

        public IntVector2 ChunkFromCell(IntVector2 cellCoordinates)
        {
            int x = cellCoordinates.X >= 0 ? cellCoordinates.X / ChunkSize.X : (cellCoordinates.X + 1) / ChunkSize.X - 1;
            int y = cellCoordinates.Y >= 0 ? cellCoordinates.Y / ChunkSize.Y : (cellCoordinates.Y + 1) / ChunkSize.Y - 1;
            return new IntVector2(x, y);
        }

        public IntVector2 CellInChunk(IntVector2 chunk, IntVector2 cellCoordinates)
        {
            int w = chunk.X * ChunkSize.X;
            int h = chunk.Y * ChunkSize.Y;
            int x = chunk.X >= 0 ? cellCoordinates.X % ChunkSize.X : (cellCoordinates.X - w);
            int y = chunk.Y >= 0 ? cellCoordinates.Y % ChunkSize.Y : (cellCoordinates.Y - h);

            return new IntVector2(x, y);
        }

        public IEnumerable<KeyValuePair<IntVector2, T>> AllChunks()
        {
            return chunks;
        }
    }
}
