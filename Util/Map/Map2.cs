﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util.Map
{
    public sealed class Map2<T> : IMap2<T>
    {
        private readonly T[] cells;

        public int Width { get; private set; }
        public int Height { get; private set; }

        public Map2(int width, int height)
        {
            Width = width;
            Height = height;

            cells = new T[Width * Height];
        }

        public Map2(int width, int height, T initialCell) : this(width, height)
        {
            for (int i = 0; i < Width * Height; ++i)
            {
                cells[i] = initialCell;
            }
        }

        public Map2(IntVector2 size) : this(size.X, size.Y) { }

        public Map2(IntVector2 size, T initialCell) : this(size.X, size.Y, initialCell) { }

        public IMap2<T> Clone()
        {
            var map = new Map2<T>(Width, Height);
            Array.Copy(cells, map.cells, cells.Length);
            return map;
        }

        public T this[int x, int y]
        {
            get => cells[x + y * Width];
            set => cells[x + y * Width] = value;
        }
        
        public T this[IntVector2 coordinates]
        {
            get => cells[coordinates.X + coordinates.Y * Width];
            set => cells[coordinates.X + coordinates.Y * Width] = value;
        }

        public IEnumerable<T> AllValues()
        {
            return cells;
        }
    }
}
