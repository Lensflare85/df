﻿using DF.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util.Map
{
    public sealed class SafeMap2<T> : IMap2<T>
    {
        private readonly T[] cells;
        private readonly T outsideCell;

        public int Width { get; private set; }
        public int Height { get; private set; }

        private SafeMap2(int width, int height)
        {
            Width = width;
            Height = height;

            cells = new T[Width * Height];
        }

        public SafeMap2(int width, int height, T outsideCell) : this(width, height)
        {
            this.outsideCell = outsideCell;
        }

        public SafeMap2(int width, int height, T outsideCell, T initialCell) : this(width, height, outsideCell)
        {
            for (int i = 0; i < Width * Height; ++i)
            {
                cells[i] = initialCell;
            }
        }

        public SafeMap2(IntVector2 size) : this(size.X, size.Y) { }

        public SafeMap2(IntVector2 size, T outsideCell) : this(size.X, size.Y, outsideCell) { }

        public SafeMap2(IntVector2 size, T outsideCell, T initialCell) : this(size.X, size.Y, outsideCell, initialCell) { }

        public IMap2<T> Clone()
        {
            var map = new SafeMap2<T>(Width, Height, outsideCell);
            Array.Copy(cells, map.cells, cells.Length);
            return map;
        }

        T GetCell(int x, int y)
        {
            if (x >= 0 && y >= 0 && x < Width && y < Height)
            {
                return cells[x + y * Width];
            }
            else
            {
                return outsideCell;
            }
        }

        T GetCell(IntVector2 location)
        {
            return GetCell(location.X, location.Y);
        }

        void SetCell(int x, int y, T cell)
        {
            if (x >= 0 && y >= 0 && x < Width && y < Height)
            {
                cells[x + y * Width] = cell;
            }
        }

        void SetCell(IntVector2 position, T cell)
        {
            SetCell(position.X, position.Y, cell);
        }

        public T this[int x, int y]
        {
            get => GetCell(x, y);
            set => SetCell(x, y, value);
        }

        public T this[IntVector2 coordinates]
        {
            get => GetCell(coordinates);
            set => SetCell(coordinates, value);
        }

        public IEnumerable<T> AllValues()
        {
            return cells;
        }
    }
}
