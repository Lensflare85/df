﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Util.Quantized
{
    public struct QAngle
    {
        private readonly int angle;

        private const int fullCircleAngle = 10000;

        private const float twoPi = (float)Math.PI * 2;

        private QAngle(int angle)
        {
            while(angle >= fullCircleAngle)
            {
                angle -= fullCircleAngle;
            }
            while(angle < 0)
            {
                angle += fullCircleAngle;
            }
            this.angle = angle;
        }

        public static readonly QAngle Zero = new QAngle(0);

        public int RawValue => angle;

        public float Radians => twoPi * angle / fullCircleAngle;
        public float Degrees => 360 * angle / fullCircleAngle;

        public static QAngle FromRadians(float radians) => new QAngle((int)(fullCircleAngle * radians / twoPi));
        public static QAngle FromDegrees(float degrees) => new QAngle((int)(fullCircleAngle * degrees / 360));

        public static QAngle operator +(QAngle a1, QAngle a2)
        {
            return new QAngle(a1.angle + a2.angle);
        }

        public static QAngle operator -(QAngle a1, QAngle a2)
        {
            return new QAngle(a1.angle - a2.angle);
        }

        public static QAngle operator *(QAngle a1, int scale)
        {
            return new QAngle(a1.angle * scale);
        }

        public static QAngle operator *(QAngle a1, float scale)
        {
            return new QAngle((int)(a1.angle * scale));
        }

        public static QAngle operator *(int scale, QAngle a1)
        {
            return new QAngle(a1.angle * scale);
        }

        public static QAngle operator *(float scale, QAngle a1)
        {
            return new QAngle((int)(a1.angle * scale));
        }

        public static QAngle operator /(QAngle a1, int scale)
        {
            return new QAngle(a1.angle / scale);
        }

        public static QAngle operator /(QAngle a1, float scale)
        {
            return new QAngle((int)(a1.angle / scale));
        }
    }
}
