﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public struct IntVector2
    {
        public readonly int X, Y;

        public Vector2 Vector2 => new Vector2(X, Y);
        public IntVector2 Direction9State => new IntVector2(X > 0 ? 1 : X < 0 ? -1 : 0, Y > 0 ? 1 : Y < 0 ? -1 : 0);

        public IntVector2(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public IntVector2(Vector2 v, Func<float, int> conversion = null)
        {
            if (conversion == null)
            {
                X = (int)v.X;
                Y = (int)v.Y;
            }
            else
            {
                X = conversion(v.X);
                Y = conversion(v.Y);
            }
        }

        public static readonly IntVector2 Zero  = new IntVector2(0, 0);
        public static readonly IntVector2 One   = new IntVector2(1, 1);
        public static readonly IntVector2 UnitX = new IntVector2(1, 0);
        public static readonly IntVector2 UnitY = new IntVector2(0, 1);

        public override int GetHashCode()
        {
            unchecked //overflow isn't an issue for hash calculation
            {
                //using prime numbers:
                int hash = 465611;
                hash = (hash * 16777619) ^ X.GetHashCode(); //TODO: GetHashCode() might be platform specific. Find something not platform specific.
                hash = (hash * 16777619) ^ Y.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(Object obj)
        {
            if (obj is IntVector2 intVector2)
            {
                return this == intVector2;
            }
            else
            {
                return false;
            }
        }

        public static bool operator ==(IntVector2 v1, IntVector2 v2)
        {
            return v1.X == v2.X && v1.Y == v2.Y;
        }

        public static bool operator !=(IntVector2 v1, IntVector2 v2)
        {
            return v1.X != v2.X || v1.Y != v2.Y;
        }

        public static IntVector2 operator -(IntVector2 v)
        {
            unchecked
            {
                return new IntVector2(-v.X, -v.Y);
            }
        }

        public static IntVector2 operator +(IntVector2 v1, IntVector2 v2)
        {
            unchecked
            {
                return new IntVector2(v1.X + v2.X, v1.Y + v2.Y);
            }
        }

        public static IntVector2 operator -(IntVector2 v1, IntVector2 v2)
        {
            unchecked
            {
                return new IntVector2(v1.X - v2.X, v1.Y - v2.Y);
            }
        }

        public static Vector2 operator +(IntVector2 v1, Vector2 v2)
        {
            unchecked
            {
                return new Vector2(v1.X + v2.X, v1.Y + v2.Y);
            }
        }

        public static Vector2 operator -(IntVector2 v1, Vector2 v2)
        {
            unchecked
            {
                return new Vector2(v1.X - v2.X, v1.Y - v2.Y);
            }
        }

        public static Vector2 operator +(Vector2 v1, IntVector2 v2)
        {
            unchecked
            {
                return new Vector2(v1.X + v2.X, v1.Y + v2.Y);
            }
        }

        public static Vector2 operator -(Vector2 v1, IntVector2 v2)
        {
            unchecked
            {
                return new Vector2(v1.X - v2.X, v1.Y - v2.Y);
            }
        }

        public static IntVector2 operator *(IntVector2 v1, IntVector2 v2)
        {
            unchecked
            {
                return new IntVector2(v1.X * v2.X, v1.Y * v2.Y);
            }
        }

        public static Vector2 operator *(IntVector2 v1, Vector2 v2)
        {
            unchecked
            {
                return new Vector2(v1.X * v2.X, v1.Y * v2.Y);
            }
        }

        public static Vector2 operator *(Vector2 v1, IntVector2 v2)
        {
            unchecked
            {
                return new Vector2(v1.X * v2.X, v1.Y * v2.Y);
            }
        }

        public static IntVector2 operator *(IntVector2 v, int scale)
        {
            unchecked
            {
                return new IntVector2(v.X * scale, v.Y * scale);
            }
        }

        public static Vector2 operator *(IntVector2 v, float scale)
        {
            unchecked
            {
                return new Vector2(v.X * scale, v.Y * scale);
            }
        }

        public static IntVector2 operator /(IntVector2 v1, IntVector2 v2)
        {
            unchecked
            {
                return new IntVector2(v1.X / v2.X, v1.Y / v2.Y);
            }
        }

        public static Vector2 operator /(IntVector2 v1, Vector2 v2)
        {
            unchecked
            {
                return new Vector2(v1.X / v2.X, v1.Y / v2.Y);
            }
        }

        public static Vector2 operator /(Vector2 v1, IntVector2 v2)
        {
            unchecked
            {
                return new Vector2(v1.X / v2.X, v1.Y / v2.Y);
            }
        }

        public static IntVector2 operator /(IntVector2 v, int scale)
        {
            unchecked
            {
                return new IntVector2(v.X / scale, v.Y / scale);
            }
        }

        public static Vector2 operator /(IntVector2 v, float scale)
        {
            unchecked
            {
                return new Vector2(v.X / scale, v.Y / scale);
            }
        }

        public float Length()
        {
            unchecked
            {
                return (float)Math.Sqrt(X * X + Y * Y);
            }
        }

        public int LengthSquared()
        {
            unchecked
            {
                return X * X + Y * Y;
            }
        }

        public float DistanceTo(IntVector2 v)
        {
            unchecked
            {
                int dx = X - v.X;
                int dy = Y - v.Y;
                return (float)Math.Sqrt(dx * dx + dy * dy);
            }
        }

        public int DistanceSquaredTo(IntVector2 v)
        {
            unchecked
            {
                int dx = X - v.X;
                int dy = Y - v.Y;
                return dx * dx + dy * dy;
            }
        }

        public override string ToString()
        {
            return $"({X},{Y})";
        }
    }
}
