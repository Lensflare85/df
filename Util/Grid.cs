﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public static class Grid
    {
        public static IEnumerable<IntVector2> CoordinatesInSize(IntVector2 size)
        {
            for (int y = 0; y < size.Y; ++y)
            {
                for (int x = 0; x < size.X; ++x)
                {
                    yield return new IntVector2(x, y);
                }
            }
        }

        public static IEnumerable<IntVector2> CoordinatesInRange(IntVector2 offset, IntVector2 size)
        {
            var xStart = offset.X;
            var yStart = offset.Y;
            var xEnd = xStart + size.X;
            var yEnd = yStart + size.Y;
            for (int y = yStart; y < yEnd; ++y)
            {
                for (int x = xStart; x < xEnd; ++x)
                {
                    yield return new IntVector2(x, y);
                }
            }
        }

        private static IntVector2 Spiral(int n)
        {
            var r = Math.Floor((Math.Sqrt(n + 1) - 1) / 2) + 1;
            var p = (8 * r * (r - 1)) / 2;
            var en = r * 2;
            var a = (1 + n - p) % (r * 8);
            switch (Math.Floor(a / (r * 2)))
            {
                case 0: return new IntVector2((int)a-(int)r, (int)-r);
                case 1: return new IntVector2((int)r, (int)(a % en) - (int)r);
                case 2: return new IntVector2((int)r - (int)(a % en), (int)r);
                case 3: return new IntVector2((int)-r, (int)r - (int)(a % en));
            }
            return IntVector2.Zero;
        }

        public static IEnumerable<IntVector2> CoordinatesInSpiral(IntVector2 offset, int radius)
        {
            yield return offset;

            if(radius == 1)
            {
                yield break;
            }

            var sideLength = (radius - 1) * 2 + 1;

            var n = sideLength * sideLength;

            for (int i=0; i<n-1; ++i)
            {
                yield return Spiral(i) + offset;
            }
        }
    }
}
